#!/bin/bash -e

#SBATCH --job-name=kiwitest
#SBATCH --account=landcare00045
#SBATCH --time=00:60:00

#SBATCH --cpus-per-task=1
#SBATCH --mem=1GB  

module load Python-Geo/3.7.3-gimkl-2018b

srun sensitivityTest_singleC.py $1 $2 $3

