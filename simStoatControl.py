#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
#from numba import jit
#import params
import pickle
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import datetime
import calendar

##############################################################
##############################################################
#####
##
class Params(object):
    def __init__(self):
        """
        parameter class for simulations
        """
        # number of iterations to simulate
        self.iter = 200
        # SET YEARS
        self.nYears = 7
        # SET YEARS TO CALC PROBABILITY OF SUCCESS
        self.burnin = 3
        # START DATE
        self.startDate = datetime.date(2020, 12, 31)
        # Area of interest in km sq
        self.area = 30
        # suppresion threshold to stay below per km sq
        self.popThreshold = 1.0
        # set Month on which to base population growth rate
        self.reproMonth = 11         
        # pCapt
        self.recPCapt = 0.7     #0.8     #.75         
        self.adultPCapt = 0.1   #0.2   #0.15
        ## ANNUAL IMMIGRATION
        self.immigrants = 18
        # initial N
        self.N0 = self.area * 0.5 
        ## WRAPPED CAUCHY PARAMETERS
        self.rpara = np.array([0.55, 0.45])
        ## LAMBDA
        self.lambdaPara = 3.0
        ## PROB OF MAST EVENT
        self.mastProb = 1.0/6.0
#        self.mastProb = 1.0/1E10
        self.lambdaMast = 4.5
        self.immMast = 28


##############################################################
##############################################################
class BasicData(object):
    def __init__(self, params):
        """
        Object to read in data and run simulation
        """
        self.params = params
        ###################
        # Run Functions
#        self.makeArrays()
        self.makeDates()
        self.getRelWrapCauchy()

    ####################################
    # Function definitions

    def makeDates(self):
        """
        make basic dates and julian dates for simulations
        """
        # get date details for starting day
        self.nTotalYears = self.params.nYears + self.params.burnin
        self.dateArray = [self.params.startDate]
        self.nMonths = 12 * self.nTotalYears
        self.monthArray = np.zeros(self.nMonths, dtype=int)
        self.monthArray[0] = self.params.startDate.month 
        self.yearArray = np.zeros(self.nMonths, dtype=int)
        self.yearArray[0] = self.params.startDate.year 
        self.piMonthLong = np.zeros(self.nMonths) 
        self.piMonthLong[0] = self.monthArray[0] / 12.0 * 2.0 * np.pi
        self.recruitIndx = np.repeat(np.arange(self.nTotalYears), 12)
        self.date_i = self.params.startDate
#        print(self.date_i, 'mon', self.monthArray[0], 'pi', self.piMonthLong[0],
#            'rec', self.recruitIndx[0])
        for i in range(1, self.nMonths):
            self.date_i = self.add_months()
            mon_i = self.date_i.month
            self.monthArray[i] = mon_i
            self.piMonthLong[i] = mon_i / 12.0 * 2.0 * np.pi
            self.yearArray[i] = self.date_i.year
#            print(self.date_i, 'mon', mon_i, 'pi', self.piMonthLong[i], 
#                'rec', self.recruitIndx[i])
            self.dateArray.append(self.date_i)
        ## STORAGE ARRAY FOR N
        self.N_2D = np.zeros((self.params.iter, self.nMonths))
        self.remove2D = np.zeros((self.params.iter, self.nMonths))
        self.recruit2D = np.zeros((self.params.iter, self.nMonths))
        self.piMonth = self.piMonthLong[:12]
#        print('pimonth', self.piMonth)

    def add_months(self):
        month = self.date_i.month
        year = self.date_i.year + month // 12
        month = month % 12 + 1
        day = calendar.monthrange(year,month)[1]
        return datetime.date(year, month, day)


    def getRelWrapCauchy(self):
        e_num = np.exp(-2*self.params.rpara[1])
        e_denom = 2 * np.exp(-self.params.rpara[1])
        sinh_rho = (1 - e_num) / e_denom
        cosh_rho = (1 + e_num) / e_denom
        cos_mu_th = np.cos(self.piMonth - self.params.rpara[0])
        dc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
        self.relWrapCauchy = dc / np.sum(dc)
        print('relWrapCau', self.relWrapCauchy)


class Simulate(object):
    def __init__(self, params, basicdata):
        """
        Class and functions to simulate population and trapping dynamics
        """
        # instances of classes
        self.params = params
        self.basicdata = basicdata
        self.iterationLooper()

    def iterationLooper(self):
        """
        loop through iterations
        i = iter; j = session; 
        """
        for i in range(self.params.iter):
            ## GET INITIAL POPULATION 
            N_ijk = np.random.poisson(self.params.N0)
            cc = 0
            ## LOOP YEARS
            for j in range(self.basicdata.nTotalYears):
                mast_j = np.random.binomial(1, self.params.mastProb)
                if mast_j == 1:
                    insituRecruits = N_ijk * self.params.lambdaMast
                    recruit_ijk = (self.basicdata.relWrapCauchy * 
                        (insituRecruits + self.params.immMast)) 
                else:
                    insituRecruits = N_ijk * self.params.lambdaPara
                    recruit_ijk = (self.basicdata.relWrapCauchy * 
                        (insituRecruits + self.params.immigrants)) 
                cc_month = 0
                ## LOOP MONTHS IN YEAR
                for k in range(12):
                    nYoung = recruit_ijk[cc_month] 
                    
                    self.basicdata.recruit2D[i, cc] = nYoung
                    removeYoung = np.random.binomial(nYoung, self.params.recPCapt)
                    removeAdult = np.random.binomial(N_ijk, self.params.adultPCapt)
                    totalRemove = removeYoung + removeAdult
                    self.basicdata.remove2D[i, cc] = totalRemove
                    N_ijk = nYoung + N_ijk - totalRemove                    
                    self.basicdata.N_2D[i, cc] = np.random.poisson(N_ijk)
                    print('i', i, 'year', j, 'mon', k, 'N', N_ijk, 'rec', 
                        np.round(nYoung, 0), 'rem', totalRemove, 
                        'den', (N_ijk / self.params.area),
                        'cc_mon', cc_month, 'cc', cc)              
                    ## UPDATE COUNTERS
                    cc_month += 1
                    cc += 1

class Results(object):
    def __init__(self, params, basicdata, simulate, kiwipath):
        """
        Class and functions to process results
        """
         ################
        # Call functions
        self.wrapperResults(params, basicdata, simulate, kiwipath)

        ################
    def wrapperResults(self, params, basicdata, simulate, kiwipath):
        """
        wrapper function to call simulate functions
        """
        # instances of classes
        self.params = params
        self.basicdata = basicdata
        self.simulate = simulate
        self.kiwipath = kiwipath
        self.calcProbSuccess()
#        self.calcSummaryStat()
#        self.plotPop_Remove()
        self.plot_1_iter()

    def calcProbSuccess(self):
        """
        calc prob of success keeping pop below threshold
        """
        self.density2D = self.basicdata.N_2D / self.params.area
        self.failMask = self.density2D > self.params.popThreshold
        self.failArray = np.sum(self.failMask, axis = 1)
        self.successMask = self.failArray == 0
        self.probSuccess = np.sum(self.successMask) / self.params.iter
        iterMax = np.max(self.density2D)
        print('prob success', self.probSuccess, 
                '95th Quant of Max', mquantiles(iterMax, prob = 0.95))
        print('array', self.failArray) 
#        print('nstorage', self.simulate.nStorage[:2])

    def calcSummaryStat(self):
        """
        get mean n, removed, recruit
        """
        self.meanN = np.mean(self.simulate.nStorage, axis = 0)
        self.meanRemove = np.mean(self.simulate.removeStorage, axis = 0)               
        self.meanRecruit = np.mean(self.simulate.recruitStorage, axis = 0)

#        print('meanN', self.meanN)


    def plotPop_Remove(self):
        """
        plot mean pred population size, recruits, removed and TN 
        """
        # make figure
        P.figure(figsize=(14, 6))
        ax = P.gca()
        lns1 = ax.plot(self.basicdata.dateArray, self.meanN, label = 'Pop size', color = 'k', linewidth = 3)
        lns2 = ax.plot(self.basicdata.dateArray, self.meanRemove, label = 'Cats removed', color = 'r', linewidth = 3)
        lns3 = ax.plot(self.basicdata.dateArray, self.meanRecruit, label = 'New recruits', color = 'b', linewidth = 3)
        P.axhline(y=self.params.popThreshold, color = 'k', linestyle = '--')
#        ax2 = ax.twinx()
#        lns4 = ax2.plot(dates, self.TNSession, label = 'Trap nights', color = 'y', linewidth = 3)
#        lns = lns1 + lns2 + lns3 + lns4
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        ax.set_ylim([0, 80])
        minDate = datetime.date(2014, 11, 25)
        maxDate = datetime.date(2024, 12, 5)
        ax.set_xlim(minDate, maxDate)
#        ax2.set_ylim(0, 8400)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_ylabel('Number', fontsize = 15)
        ax.set_xlabel('Years (January)', fontsize = 15)
#        ax2.set_ylabel('Weekly trap nights', fontsize = 17)
        fileName = 'sim_Scenario' + str(self.params.scenario) + '.png'
        plotFname = os.path.join(self.kiwipath, fileName)
        P.savefig(plotFname, format='png')
        P.show()


    def plot_1_iter(self):
        """
        plot mean pred population size, recruits, removed and TN
        """
        # make figure
        P.figure(figsize=(14, 6))
        ax = P.gca()
        lns1 = ax.plot(self.basicdata.dateArray, self.basicdata.N_2D[0], 
            label = 'Number of stoats after trapping', color = 'k', linewidth = 3)
        lns2 = ax.plot(self.basicdata.dateArray, self.basicdata.remove2D[0], 
            label = 'Stoats removed', color = 'r', linewidth = 3)
        lns3 = ax.plot(self.basicdata.dateArray, self.basicdata.recruit2D[0], 
            label = 'Recruits & immigrants', color = 'b', linewidth = 1.5)
        P.axhline(y=(self.params.popThreshold * self.params.area), color = 'k', 
            linestyle = '--')
#        ax2 = ax.twinx()
#        lns4 = ax2.plot(dates, self.TNSession, label = 'Trap nights', color = 'y', linewidth = 3)
#        lns = lns1 + lns2 + lns3 + lns4
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper right')
        ax.set_ylim([0, 40])
        minDate = datetime.date(2020, 11, 1)
        maxDate = datetime.date(2031, 1, 10)
        ax.set_xlim(minDate, maxDate)
#        ax2.set_ylim(0, 8400)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(16)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(16)
        ax.set_ylabel('Number', fontsize = 16)
        ax.set_xlabel('Years (January)', fontsize = 16)
#        ax2.set_ylabel('Weekly trap nights', fontsize = 17)
        fileName = 'simTrapStoat4.png'
        plotFname = os.path.join(self.kiwipath, fileName)
        P.savefig(plotFname, format='png')
        P.show()

            
########            Main function
#######
def main():

    kiwipath = os.getenv('KIWIPROJDIR', default = '.')

    params = Params()

    basicdata = BasicData(params)

    simulate = Simulate(params, basicdata)

    results = Results(params, basicdata, simulate, kiwipath)

if __name__ == '__main__':
    main()



