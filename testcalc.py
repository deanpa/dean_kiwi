#!/usr/bin/env python

import os
from kiwimodel import calculation
from kiwimodel import preProcessing

preProcessDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'testresults1', 'test.pkl')
data = preProcessing.KiwiData.unpickleFromFile(preProcessDataPath)
results = calculation.runModel(data)

resultsDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'testresults1', 'results.pkl')
results.pickleSelf(resultsDataPath)

