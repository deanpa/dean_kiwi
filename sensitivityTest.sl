#!/bin/bash

#SBATCH --job-name=kiwitest
#SBATCH --account=landcare00045
#SBATCH --mail-type=end
#SBATCH --time=00:8:00

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=8000  

module load Python-Geo/3.7.3-gimkl-2018b

srun sensitivityTest_single.py $1 $2 $3

