fullExtent.shp : Shapefile of full extent, at 40 km buffer around region 3. 
                    Clipped at the coast.


lcdb_extent.tif : lcdb raster created from lcdb shapefile using gdal_rasterize.
                    The lcdb shapefile was clipped in QGIS to full extent.

lcdb_kea_indx.img: was produced from lcdb_extent.tif in the reclassLCDB.py script. 
                    The raster values are indices 0 - 7.

dem_N_SI.tif : created from the DEM tiles downloaded and found in /AA_Mega_Local/GIS_Data
                using the script mosaicRaster.py in dean_kiwi.


dem_Region3.tif : clipped dem to region 3 using gdalwarp as follows:

gdalwarp -s_srs EPSG:2193 -t_srs EPSG:2193 -te 1359269.677 5160959.543 1649669.677 5446959.543 -tr 200 -200 -r bilinear -of Gtiff -dstnodata 0.0 /home/dean/AA_Mega_Local/GIS_Data/DEM/NorthernSouthIsland/dem_N_SI.tif kea/keaData/dem_region3.tif


