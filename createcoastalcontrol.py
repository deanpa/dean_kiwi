#!/usr/bin/env python

import sys
import numpy
import argparse
from rios import applier

def createCoastalCtrl_RIOS(info, inputs, outputs):

    mask = (inputs.dem[0] < 200) & (inputs.dem[0] > 0)    
    coastalctrl = numpy.where(mask, 1, 0).astype(numpy.uint8)

    # are we restricting by shp files?
    if hasattr(inputs, 'shpList'):
        # create a mask of the pixels that are in one of the controls
        ctrlMask = numpy.zeros_like(coastalctrl, dtype=numpy.bool)
        
        for shp in inputs.shpList:
            ctrlMask |= (shp[0] > 0)

        coastalctrl[~ctrlMask] = 0

    outputs.coastalctrl = numpy.expand_dims(coastalctrl, 0)
    

def createCoastalCtrl(dem, output, shpList):

    infiles = applier.FilenameAssociations()
    infiles.dem = dem

    if shpList is not None and len(shpList) > 0:
        infiles.shpList = shpList

    outfiles = applier.FilenameAssociations()
    outfiles.coastalctrl = output

    applier.apply(createCoastalCtrl_RIOS, infiles, outfiles)


def getCmdArgs():
    """
    Get commandline arguments
    """
    p = argparse.ArgumentParser()
    p.add_argument('-d', '--dem', help="Input DEM", required=True)
    p.add_argument('-o', '--output', help="Output File", required=True)
    p.add_argument('-c', '--control', nargs='*',
        help="List of shapefiles to use in restricting the extent of the coastal control")

    cmdargs = p.parse_args()

    return p.parse_args()


if __name__ == '__main__':

    cmdargs = getCmdArgs()
    createCoastalCtrl(cmdargs.dem, cmdargs.output, cmdargs.control)

##  call on command line:

## NESI BEST CALL
#./createcoastalcontrol.py --dem=$POFPROJDIR/kiwi_data/dem.tif --output=$POFPROJDIR/kiwi_data/coastalctrl.img --control $POFPROJDIR/kiwi_data/SOIK_1080_Freeman_Burn_\&_surrounds_2017-05-22.shp $POFPROJDIR/kiwi_data/SOIK_1080_Mt_Forbes_wider_2017-05-20.shp $POFPROJDIR/kiwi_data/SOIK_1080_West_Cape_2017-05-20.shp $POFPROJDIR/kiwi_data/SOIK_1080_Wet_Jacket_Peninsulas_2017-05-22.shp




## BEST CALL
#./createcoastalcontrol.py --dem=kiwi_data/dem.tif --output=kiwi_data/coastalctrl.img --control kiwi_data/SOIK_1080_Freeman_Burn_\&_surrounds_2017-05-22.shp kiwi_data/SOIK_1080_Mt_Forbes_wider_2017-05-20.shp kiwi_data/SOIK_1080_West_Cape_2017-05-20.shp kiwi_data/SOIK_1080_Wet_Jacket_Peninsulas_2017-05-22.shp



