#!/usr/bin/env python

import numpy as np
#from scipy import stats
import pylab as P
from scipy.stats.mstats import mquantiles

def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D




class PDetect():
    def __init__(self):
#        self.g0Range = [0.01, 0.15]
#        self.sigmaRange = [30.0, 200.0]
#        self.n = 1



#        self.iter = 1000
        self.den = 12         # density per 4 ha or 40,000 m2
        ########## SOP only ##########
#        self.lineSpace = 300.0
#        self.ttSpace = 50.0
        nTT_Line = 10.0
        nLines = 12.0
        ##############################
        self.ntraps = nTT_Line * nLines
#        self.xTT = 99.5
#        self.yTT = 99.5

        self.g0 = 0.02
        self.sigma = 25.0
        self.nNights = 4
        

        ####
        # run functions
        self.makeLocations()


        # end functions
        ####

    def makeLocations(self):
        TTArea = np.pi * ((4.0*self.sigma)**2)
        den_m2 = self.den / 40000
        nRats = np.int(np.round((den_m2 * TTArea), 0))
        print('TTArea', TTArea, 'nRats', nRats, 'den_m2', den_m2)

        dist = np.random.uniform(0.0, 4.0*self.sigma, nRats)

        pdect = self.g0 * np.exp(-(dist**2) / 2.0 / (self.sigma**2))

        PD = 1.0 - np.prod((1.0 - pdect)**self.nNights)
        print('PD', PD)
#        ## Math PD
#        mathPD = 1.0 - np.exp(-2.0 * np.pi * self.g0 * (self.sigma**2) * den_m2)
#        print('sim pd', PD, 'MATH PD Mean = ', mathPD)

        tci = np.random.binomial(self.ntraps, PD, 10) / self.ntraps
        print('Tracking rate', tci)


    
########            Main function
#######
def main():

    pdetect = PDetect() 


if __name__ == '__main__':
    main()
