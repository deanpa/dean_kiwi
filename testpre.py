#!/usr/bin/env python

import os
import numpy as np
from kiwimodel import params
from kiwimodel import preProcessing
from kiwimodel import diagnosticSim

pars = params.KiwiParams()

# set paths to scripts and data
pars.inputDataPath = os.path.join(os.getenv('POFPROJDIR', default='.'), 'kiwi_data')
pars.outputDataPath = os.path.join(os.getenv('POFPROJDIR', default='.'), 'testresults1')
if not os.path.isdir(pars.outputDataPath):
    os.mkdir(pars.outputDataPath)

### SET DATA AND PATHS TO DIRECTORIES
pars.setExtentShapeFile(os.path.join(pars.inputDataPath, 'FiordlandConArea.shp'))
pars.setKClasses(os.path.join(pars.inputDataPath, 'lcdb_lowbeech.img'))
pars.setDEM(os.path.join(pars.inputDataPath, 'dem.tif'))
pars.setResolutions((200.0, 1000.0, 1000.0))
pars.setControlFile(os.path.join(pars.inputDataPath, 'control1.csv'))
pars.setControlPathPrefix(pars.inputDataPath)

### SET YEARS AND BURN IN YEARS
pars.setBurnin(70)
pars.setYears(np.arange(20))
### SET ITERATIONS
pars.setIterations(200)


# Control parameters
pars.setReactiveMode(0.2)

### Masting parameters
pars.setMastRho(16000.0)
#pars.setMastWindowSize(130)
pars.setMastCellParams(0.001, 1000.0)
#pars.setMastProportionParams(0.4, 0.2)
pars.setMastSpatialSD(2.1)
pars.setMastPrEvent(1.0 / 6.0)

## rodent parameters
pars.setPRodentPresence(0.95)
pars.setRodentInitialMultiplier(.75)
pars.setRodentProbEatBait(0.8)
pars.setRodentGrowthRate(1.2)
pars.setPrpGrowRateControl(0.2)

## stoat parameters
pars.setPStoatPresence(0.7)
pars.setStoatInitialMultiplier(.7)
pars.setStoatGrowthRate(1.2)
pars.setStoatKAsymptotes(1.0, 12.0)
pars.setPEncToxic(0.006)  #.004        # operates at stoat scale
pars.setPEatEncToxic(0.9)

### Kiwi parameters
pars.setKiwiK(20)
pars.setPKiwiPresence(0.7)
pars.setKiwiInitialMultiplier(0.1)
pars.setKiwiPsi(0.02)   #.0075
pars.setMinPredationSurv(0.88)
pars.setKiwiGrowthRate(.12)
pars.setKiwiPopSD(.12)

data = preProcessing.KiwiData(pars)
data.pickleSelf(os.path.join(pars.outputDataPath, 'test.pkl'))



## DIAGNOSTIC SIMULATION; plot and save graph
#simDiag = diagnosticSim.OnePixelSim(pars)



