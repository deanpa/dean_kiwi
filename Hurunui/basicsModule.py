#!/usr/bin/env python

import numpy as np 
from numba import jit
import datetime
from scipy.special import gammaln
from scipy.special import gamma



def thProbFX(tt, debug = False):
    tt2 = np.exp(tt)
    tt3 = tt2/np.sum(tt2)
    return(tt3)

def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

#@jit
def matrixsub(arr1, arr2):
    ysize = arr1.shape[0]
    xsize = arr2.shape[0]
    out = np.empty((ysize, xsize), arr1.dtype)
    for y in range(ysize):
        for x in range(xsize):
            out[y,x] = arr1[y] - arr2[x]
    return out

def distmat(x1, y1, x2, y2):
    dx = matrixsub(x1, x2)
    dy = matrixsub(y1, y2)
    dmat = np.sqrt(dx**2.0 + dy**2.0)
    return dmat


def initialPPredatorTrapCaptFX(basicdata, availTrapNights, location, g0Param, debug = False):      
    """
    # prob that predator was capt in trap
    """
    distToTraps = basicdata.distTrapToCell2[:, location]      # dist from cell j to all traps
    eterm = np.exp(-(distToTraps) / basicdata.var2)           # prob predator-trap pair
    pNoCapt = 1. - g0Param * eterm
    pNoCaptNights = pNoCapt**(availTrapNights)
    pNoCaptNights[pNoCaptNights >= .9999] = 0.9999
    pcapt = 1 - pNoCaptNights
    pcapt[pcapt > .97] = 0.97
    if debug == True:
        print("basicdata.distTrapToCell2.shp", basicdata.distTrapToCell2.shape)
        print("distToTraps.shp", distToTraps.shape)
    return pcapt


def removeDatFX(nsession, predator, session):
    removeDat = np.arange(nsession)
    for i in range(nsession):
        removeDat[i] = np.sum(predator[session==i])
    return(removeDat)


def multinomial_pmf(probs, counts):
    probssum = probs.sum()
    if probssum < 0.999 or probssum > 1.0001:
        raise ValueError("probs must sum to 1")
    if probs.size != counts.size:
        raise ValueError("probs and counts must be the same size")
    return gammaln(counts.sum() + 1.0) - gammaln(counts + 1.0).sum() + np.sum(np.log(probs)*counts)


def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    Wikipedia pdf equation
    """
#    e_num = np.exp(-2*rho)
#    e_denom = 2 * np.exp(-rho)
#    sinh_rho = (1 - e_num) / e_denom
#    cosh_rho = (1 + e_num) / e_denom
    sinh_rho = np.sinh(rho)
    cosh_rho = np.cosh(rho)
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc


def calcRelWrapCauchy(wrp_rpara, nPseudoJulYear, uYearIndx, pseudoYearRecruitIndx, daypiRecruit):
    """
    Calc the rel cauchy value for all sessions for distributing recruits
    """
    relWrpCauchy = np.zeros(nPseudoJulYear)
    for i in uYearIndx:
        yearMask = pseudoYearRecruitIndx == i
        # day pi in year i
        daypiTmp = daypiRecruit[yearMask]
        # pdf of daypi in year i
        dc = dwrpcauchy(daypiTmp, wrp_rpara[0], wrp_rpara[1])
        # mask of recruit window in year i
        reldc = dc / np.sum(dc)     #  dc.sum()
        relWrpCauchy[yearMask] = reldc
    return relWrpCauchy


       
def g0Loop(g0All, nsession, trapSession, g01, g0MultiplierAll):
    logit_g0 = logit(g01)
    g0All_3 = g0All.copy()
    for k in range(nsession):
        sessmask = trapSession == k                            # sess mask for trap data
        # get logit g0 for that bait-trap by season effect
        logit_g0All_1 = logit_g0 + g0MultiplierAll[k]
        g0All_2 = inv_logit(logit_g0All_1)
        g0All_3[sessmask] = g0All_2
    return g0All_3


def g0AllFX(g0_2D, g0All, trapBaitIndx, nsession, trapSession, g0MultiplierAll):
    """
    make a long array of g0 values associated with trap data for each bait trap type
    and repeat for each session, with season multiplier
    """
    # expand by trap-bait type to length of self.nTraps
    g01 = g0_2D[trapBaitIndx]
    # loop thru sessions
    g0All_3 = g0Loop(g0All, nsession, trapSession, g01, g0MultiplierAll)
    return g0All_3



def getTrapSummary(tna, trapped, tbt, nsess):
    """
    function to explore trapbaittype captures summaries and trapnights
    """
    tbtAll = np.tile(tbt, nsess)
    utbt = np.unique(tbt)
    print('trapNightsAvail', len(tna), tna[0:100])
    print('trapped', len(trapped), trapped[0:100])
    print('trapBaitIndx', len(tbtAll), tbtAll[0:100])
    print('utbt', utbt)
    ntbt = len(utbt)
    tnaRes = np.empty(ntbt)
    trapRes = np.empty(ntbt)
    for i in utbt:
        tnaRes[i] = np.sum(tna[tbtAll == i])
        trapRes[i] = np.sum(trapped[tbtAll == i])
    trap_tna = trapRes / tnaRes 
    ResTab = np.hstack([np.expand_dims(utbt, 1), np.expand_dims(trapRes, 1), np.expand_dims(tnaRes,1), np.expand_dims(trap_tna, 1)])
    print(ResTab)
                                



############################################################################
######      MCMC NUMBA FUNCTIONS


### NOT USED
@jit
def getSessionRecruits(reproPop, totalRecruits, yearRecruitIndx, relWrpCauchy, sessionRecruits,
        sessionJul, maxSessionJul):
    """
    loop thru sessions to get n recruits for all days, then populate the sessionRecruits array
    """
    sumRecruits = 0.0
    sessionCounter = 1
    for i in range(1, (maxSessionJul + 1)):
        yrIndx = yearRecruitIndx[i]
        recruits_i =  totalRecruits[yrIndx] * relWrpCauchy[i]
        sumRecruits += recruits_i
        if (sessionJul[sessionCounter] == i):
            sessionRecruits[sessionCounter] = sumRecruits
            sessionCounter += 1
            sumRecruits = 0.0
    return sessionRecruits

@jit
def getSessRecruitsOneYear(nPseudoSessInYear, pseudoSessRecruitYear, 
        sessionRecruits, sessionJul, julPseudoYear, nSession):
    """
    loop thru sessions to get n recruits for all days, then populate the sessionRecruits array
    """
    sumRecruits = 0.0
    sessionCounter = 0
#    carryOn = 1
#    i = 0
    for i in range(nPseudoSessInYear):
#    while carryOn == 1:
        sumRecruits += pseudoSessRecruitYear[i]      # recruits_i
        if(sessionJul[sessionCounter] == julPseudoYear[i]):
            sessionRecruits[sessionCounter] = sumRecruits
            sessionCounter += 1
            sumRecruits = 0.0
            if sessionCounter == nSession:
                break
#                carryOn = 0
#        i += 1
    return sessionRecruits


#        if debug == True:
#            if i < 500:
#                print('i', i, 'sessJul', sessionJul[sessionCounter], 
#                    'pseudoJul', julPseudoYear[i], 'sessRecruits', sessionRecruits[sessionCounter],
#                    'pseudoRec', pseudoSessRecruitYear[i], 'sumRecruits', sumRecruits) 


class BasicData(object):
    def __init__(self, predatortrapdata, covFname, params):
        """
        Object to read in cat, trap and covariate-cell data
        Import updatable params from params
        """
        self.params = params

        print('ngibbs =', self.params.ngibbs, 'thinrate =', self.params.thinrate,
            'burnin = ', self.params.burnin) 

        self.predatortrapdata = predatortrapdata
        # move predatortrapdata variables into basicdata
        self.session = self.predatortrapdata.week - 1
        self.session = self.session.astype(int)
#        self.week = self.predatortrapdata.week.copy()
#        self.uweek = np.unique(self.week)
        self.captTrapID = self.predatortrapdata.captTrapID
        self.julianStart = self.predatortrapdata.julianStart
        self.julianYear = self.predatortrapdata.julianYear
        self.captTrapBait = self.predatortrapdata.ttypebait
        self.ttype = self.predatortrapdata.ttype

#        print('julianYear', self.julianYear[:365], 'len jul', len(self.julianYear))


        # get trap data
        self.getTrapData()
#        self.trapped = self.predatortrapdata.cattrap
        self.nDays = self.predatortrapdata.nDays
        self.year = self.predatortrapdata.year
        # passed in here to pass on to gibbsobj pickle for postProcessing
        self.fortnightMask = self.predatortrapdata.fortnightMask
        self.sessionIntervalMask = self.predatortrapdata.sessionIntervalMask
        ########################################## session data for results
        self.sessionYear = self.predatortrapdata.sessionYear
        self.sessionMonth = self.predatortrapdata.sessionMonth
        self.sessionDay = self.predatortrapdata.sessionDay

        print('mindate', self.sessionDay[0], self.sessionMonth[0], self.sessionYear[0])
        print('maxdate', self.sessionDay[-1], self.sessionMonth[-1], self.sessionYear[-1])
#        print('self.sessionIntervalMask', self.sessionIntervalMask, len(self.sessionIntervalMask))
#        print('self.fortnightMask', self.fortnightMask, len(self.fortnightMask))
#        print('self.uweek', self.uweek, len(self.uweek))

        ###########################################  Trap data
        # data associated with trap location data
        self.trapX = self.predatortrapdata.trapX
        self.trapY = self.predatortrapdata.trapY
        self.trapID = self.predatortrapdata.trapTrapid
        # number of traps
        self.nTraps = self.predatortrapdata.nTraps
        self.trapBaitIndx = self.predatortrapdata.trapBaitIndx   # index for applying g0 across trap-bait types

#        self.testCapt_TrapData()

        #############################################
        # new associated variables
        self.uSession = np.unique(self.session)
        self.nsession = len(self.uSession)   # np.int(max(self.session + 1))
        self.ndat = len(self.session)
        self.uniqueCaptTrapBait = np.unique(self.captTrapBait)  # unique trap-bait combos (16)
        self.nTrapBait = len(self.uniqueCaptTrapBait)       # number of combos (16)
        self.uJulian = np.unique(self.julianYear)
        self.zeroPCapt = np.zeros(self.nTraps)              # template of zeros to get pcapt in 3 traps


#        print('julStart', self.julianStart[:100])
#        print('self.week', self.week)



#        print('trapped', np.sum(self.trapped), self.trapped[self.trapped>0])

        ###################################################
        ################################################          Grid-cell covariate data
        # covariate data by 1km grid cells.
        self.covDat = np.genfromtxt(covFname, delimiter=',', names=True,
            dtype=['f8', 'f8', 'f8'])
        self.cellX = self.covDat['x']
        self.cellY = self.covDat['y']

        self.eastCov = self.cellX - np.min(self.cellX)
        self.northCov = self.cellY - np.min(self.cellY)
        self.tuss = self.covDat['hab']
        self.ncell = len(self.eastCov)

        # Covariates for habitat model       
        self.scaleEast = (self.eastCov - np.mean(self.eastCov)) / np.std(self.eastCov)
        self.scaleNorth = (self.northCov - np.mean(self.northCov)) / np.std(self.northCov)
        self.scaleTuss = (self.tuss - np.mean(self.tuss)) / np.std(self.tuss)
        self.scaleCatFerrets = np.zeros(self.ncell) 

        # stack and make covariate array
        xdatFull = np.hstack([np.expand_dims(self.scaleEast,1),
                            np.expand_dims(self.scaleNorth,1),
                            np.expand_dims(self.scaleTuss,1),
                            np.expand_dims(self.scaleCatFerrets, 1)])
        if self.params.species == 'Stowea':
            predXDat = np.hstack([xdatFull[:, :3], xdatFull[:, :3]])
            xTmp = np.dot(predXDat, self.params.catFerretPara)
            xTmp = (xTmp - np.mean(xTmp)) / np.std(xTmp)
            xdatFull[:, -1] = xTmp
        # select covariates using index from params
        selectCov = xdatFull[:, self.params.xdatIndx]
        # 2-d array of covariates
        self.xdat = selectCov.copy()
        # number of beta parameter, including intercept
        self.nbcov = self.params.nbcov
        ## check for colinearity among covariates
        print('Covariates correlation matrix')
        print(np.round(np.corrcoef(self.xdat, rowvar = 0), 4))


        ##############
        ###############      Initial values parameters from params.py for updating
        ##############       Have in basicdata so can update, and import pickle
        # maximum number of predators from params
        self.maxN = self.params.maxN
        # sigma home range parameter
        self.sigma = self.params.sigma
        self.sigma_s = self.sigma + 0.4

        ###################  g0 preparation

        # Make "self.startJulSess": starting julian day for each week-session
        # Make "self.yearSess" : year associated with each session
        self.getWeekJulian()

        # make "self.g0_Indx_Multiplier" array indexing where high, low and standard g0-multipliers applies
        self.make_g0_Indx()


#        print('g0_Indx', self.g0_Indx_Multiplier[:365], 'len g0', len(self.g0_Indx_Multiplier))
#        print('startJulSess', self.startJulSess[:365], 'len startjul', len(self.startJulSess))

        # array of 3 for each season
        self.g0Multiplier = self.params.g0Multiplier                            # update
        self.g0Multiplier_s = self.g0Multiplier.copy()
        self.g0Multiplier_s[1:] = np.random.normal(self.g0Multiplier[1:], .001)
        # gamma priors on multiplier by season (1,1)

        # make array length nSession of multiplier by season
        self.g0MultiplierAll = self.g0Multiplier[self.g0_Indx_Multiplier]       # update when update self.g0Multiplier
        self.g0MultiplierAll_s = self.g0Multiplier_s[self.g0_Indx_Multiplier]

#        print('g0Multi', self.g0MultiplierAll[:120])

        # g0 capture parameter
        self.g0 = np.exp(np.random.normal(np.log(.02),.01, size = self.nTrapBait))
        self.g0 = np.expand_dims(self.g0, 1)

        # make indexing array for sessions that corresponds to trap data
        self.gettrapSession()

        # array length of nsession * ntraps  with g0 for the associated trap-bait type
        # incorporates seasonal effect
#        self.g0All =  np.zeros(self.nsession * self.nTraps)
        self.g0All =  np.expand_dims(np.zeros(self.nsession * self.nTraps), 1)
        self.g0All = g0AllFX(self.g0, self.g0All, self.trapBaitIndx, self.nsession, 
                        self.trapSession, self.g0MultiplierAll)


        # proposed g0_s
        self.g0_s = np.exp(np.random.normal(np.log(self.g0),.005))
        self.g0All_s =  np.expand_dims(np.zeros(self.nsession * self.nTraps), 1)
        self.g0All_s = g0AllFX(self.g0_s, self.g0All_s, self.trapBaitIndx, self.nsession, 
                        self.trapSession, self.g0MultiplierAll)

        # immigration and mortality parameter
        self.ig =  self.params.ig
        self.i_s =  self.ig + 1

        ##########################################
        #########################################   # wrp cauchy parameters for reproduction

        # growth rate parameter from params.
        self.rg = self.params.rg        
        self.rs = self.params.rg + .001
        self.initialReproPop = self.params.initialReproPop
        self.initialReproPop_s = self.initialReproPop - 1

        # wrapped cauchy parameters and function for distributing new recruits
        self.rpara = self.params.rpara
        self.rpara_s =  self.rpara + .01

        # Make "self.reproMask" mask  for identifying reproductive period
        # and "self.reproPeriodIndx" for indexing reproductive periods
        self.makeReproMask()
        # mask of daypi temporal window to distribute new recruits
        ###### Predict new recruits
        # days in radians
        self.daypi = self.startJulSess / 365 * 2 * np.pi
        # make continuous sessions for all days in entire study period
        self.makePseudoSession()
        

        # make index for year to distribute new recruits: "self.yearRecruitIndx "
        self.makeYearRecruitIndx()
#        self.uYearIndx = np.unique(self.yearRecruitIndx)    # unique year recruitment indices

#        self.maxUYearIndx = np.max(self.uYearIndx)          # max indx for recruits in 12/2014 
        self.nYear = len(self.uYearIndx)                    # number of years for reproduction
        # number of sessions in year 0. use to update latent initial repro pop.
        self.nSessYr0 = len(self.yearRecruitIndx[self.yearRecruitIndx == 0]) 

        # make array self.nSessReproPeriod used for calc mean repro pop
        self.getNSessInReproPeriod()

        # relative wrapped cauchy function for recruitment periods for all years    
#        self.relWrpCauchy = np.zeros(self.nsession, dtype = int)        
#        self.relWrpCauchy = calcRelWrapCauchy(self.rpara, self.nsession, self.uYearIndx, 
#                            self.daypi, self.yearRecruitIndx)       #, self.recruitWindowMask)
#        self.relWrpCauchy_s = calcRelWrapCauchy(self.rpara_s, self.nsession, self.uYearIndx, 
#                            self.daypi, self.yearRecruitIndx)       #, self.recruitWindowMask_s)

        # get removeDat per week session
        self.removeDat = removeDatFX(self.nsession, self.trapped, self.session)

        # get initial N and associated recruitment values
#        self.getInitialN()
        self.initialisePop()
#        print('N', self.N)
#        print('Ns', self.Ns)

        self.cellID = np.arange(self.ncell, dtype = int)
        self.nCatInCellTemplate = np.zeros(self.ncell)

        # make arrays associated with trap data
#        self.getTrapIDFX()
        self.getTrapNightsFX()

        # make matrix of distance from traps (rows) and cells (col)
        distTrapToCell = distmat(self.trapX, self.trapY, self.cellX, self.cellY)
        self.distTrapToCell2 = distTrapToCell**2.0

        # calc initial Npred and Npred_s for all sessions
        self.Npred = self.NpredAllSessFX(self.N)
        self.Npred_s = self.Npred.copy()
#        self.Npred_s = self.NpredAllSessFX(self.N + 1)
#        print('npred', self.Npred)
#        print('npred_s', self.Npred_s)
#        print(self.removeDat.dtype, self.N.dtype)

        #######################
        # habitat beta parameters and initial mu and Th values
        self.b = self.params.b
        self.lth = np.dot(self.xdat, self.b)
#        self.lth = np.random.normal(self.mu.flatten(), .5, self.ncell)
        self.thMultiNom = thProbFX(self.lth, debug = False)

        self.bs = np.random.normal(self.b, .02)
        self.lth_s = np.dot(self.xdat, self.bs)
        self.thMultiNom_s = thProbFX(self.lth_s)

#        print('self.N', self.N)

        # habitat variance parameter
        self.nsample = np.array([-3, -2, -1, 1, 2, 3])
        self.datseq = np.arange(self.params.maxN, dtype = int)
        self.var2 = 2.0 * (self.sigma**2.0)
        self.var2_s = 2.0 * (self.sigma_s**2.0)
        self.llikTh = np.empty(self.nsession)
        self.llikTh_s = np.empty(self.nsession)
        self.llikR = np.empty(self.nsession - 1)
        self.llikR_s = np.empty(self.nsession - 1)
        self.llikImm = np.empty(self.nsession - 1)
        self.llikImm_s = np.empty(self.nsession - 1)
        self.llikg0Sig = np.empty(self.nsession)
        self.llikg0Sig_s = np.empty(self.nsession)
        self.expTermMat =  np.exp(-(self.distTrapToCell2) / self.var2)
        self.expTermMat_s =  np.exp(-(self.distTrapToCell2) / self.var2_s)
        

        ##############
        ##############      # end updatable parameters



    ####################     # Basicdata functions.
    ####################

    def getTrapData(self):
        """
        # use indicator in params file to get species trap data
        """
        if self.params.species == 'Cats':
            self.trapped = self.predatortrapdata.cattrap
        elif self.params.species == 'Ferrets':
            self.trapped = self.predatortrapdata.ferretTrap
        elif self.params.species == 'Hedgehogs':
            self.trapped = self.predatortrapdata.hedgehogTrap
        elif self.params.species == 'Stowea':
            self.trapped = self.predatortrapdata.stoweaTrap
        nd = len(self.trapped)
        print('sum trapped', np.sum(self.trapped))
        

    def testCapt_TrapData(self):
        """
        # test to see if have perfect coverage between
          trap data and capture data
        """
        ucapt = np.unique(self.captTrapID)
        utrap = np.unique(self.trapID)
        ct = np.in1d(ucapt, utrap)
#        print('len capt', len(ucapt), 'n Trues', np.sum(ct))
        tc = np.in1d(utrap, ucapt)
#        print('len trap', len(utrap), 'n Trues', np.sum(tc))

    def getWeekJulian(self):
        """
        Get starting julian day and year for each week-session
        Use to make mask for reproductive period
        """
        self.startJulSess = np.zeros(self.nsession, dtype = int)
        self.yearSess = np.zeros(self.nsession, dtype = int)
        for i in range(self.nsession):
            sess = self.uSession[i]
            j1 = self.julianYear[self.session == sess]
            year = self.year[self.session == sess]
            self.startJulSess[i] = np.min(j1)        # julian year for sessions
            self.yearSess[i] = np.min(year)
#        print('self.startJulSess', self.startJulSess, len(self.startJulSess))

    def makePseudoSession(self):
        """
        # make pseudo sessions to distribute new recruits after
                survey and trapping had finished - length total days in study
        """
        
        self.getEndPseudoDate()
        endDate = self.lastPseudoDate
        
#        endDate = datetime.date(2014, 11, 20)
        end2005 = datetime.date(2005, 12, 31) 
        end2005Tuple = end2005.timetuple()
        endYearJul = end2005Tuple.tm_yday
        ddays = int(self.params.reproDaysBack[1])
        datei = end2005 - datetime.timedelta(days=ddays)
#        datei =  datetime.date(2005, 12, 1)
        timetuple = datei.timetuple()
        julYri = timetuple.tm_yday
        self.pseudoJulYear = julYri
        self.daypiRecruit = julYri / endYearJul * 2.0 * np.pi
        carryOn = 1
        while carryOn == 1:
            datei = datei + datetime.timedelta(days=1)
            # get number of days in year
            year_i = datei.year
            dateEnd = datetime.date(year_i, 12, 31)
            timeTupleEnd = dateEnd.timetuple()
            ndaysInYear = timeTupleEnd.tm_yday
            timetuple = datei.timetuple()
            julYri = timetuple.tm_yday
            recruitDayPi = julYri / ndaysInYear * 2. * np.pi
            self.daypiRecruit = np.append(self.daypiRecruit, recruitDayPi)
            self.pseudoJulYear = np.append(self.pseudoJulYear, julYri)
            if datei == endDate:
                carryOn = 0
        self.nPseudoJulYear = len(self.pseudoJulYear)
        self.pseudoSessRecruits = np.zeros(self.nPseudoJulYear)
#        print('len pseudoSessRec', len(self.pseudoSessRecruits), self.pseudoSessRecruits[:100])

    def getEndPseudoDate(self):
        """
        get last date of pseudo days; last recruitment day
        # 365 days from end of last repro period
        """
        maxReproIndx = np.max(self.reproPeriodIndx)
        lastReproMask = self.reproPeriodIndx == maxReproIndx
        years = self.sessionYear[lastReproMask]
        yr = years[-1]
        months = self.sessionMonth[lastReproMask]
        mon = months[-1]
        days = self.sessionDay[lastReproMask]
        day = days[-1]
        lastReproDate = datetime.date(yr, mon, day)
        self.lastPseudoDate = lastReproDate + datetime.timedelta(days=366)


    def makeYearRecruitIndx(self):
        """
        make indx to distribute recruits across session in a 365-day period 
        """
        relDay = np.zeros(self.nsession, dtype = int)
        # mask sessions from jan 1 upto end of repro period
        beforeMask = self.params.reproDays[-1] >= self.startJulSess
        # mask sessions from after repro period to end of December
        afterMask = self.params.reproDays[-1] < self.startJulSess
        # relative days up
        relDay[beforeMask] = 365 + self.startJulSess[beforeMask]
        relDay[afterMask] = self.startJulSess[afterMask] - self.params.reproDays[-1] 
        yrRecIndx = np.zeros(self.nsession)
        indx = 0
        for i in range(1, self.nsession):     
            if relDay[i] < relDay[i-1]:
                indx += 1
            yrRecIndx[i] = indx
#            print('sess', i, 'jul', self.startJulSess[i], 'indx', indx)
        self.yearRecruitIndx = yrRecIndx.copy()
        self.yearRecruitIndx = self.yearRecruitIndx.astype(int)
        self.uYearIndx = np.unique(self.yearRecruitIndx)
        self.nSessInReproPeriod = np.zeros(len(self.uYearIndx), dtype = int)
        for i in range(1, len(self.uYearIndx)):
            self.nSessInReproPeriod[i] = np.sum(self.reproMask[self.reproPeriodIndx == i])
        # Make indx array for recruitment periods for pseudo sessions
        self.makePseudoRecruitIndx()
#        print('yearRecruitIndx', self.yearRecruitIndx, len(self.yearRecruitIndx))
#        print('self.sessionMonth', self.sessionMonth, len(self.sessionMonth))
#        print('self.sessionYear', self.sessionYear, len(self.sessionYear))
#        print('sessions', self.uSession, self.nsession)   
#        print('nSessInRepro', self.nSessInReproPeriod)
#        print('uYearIndx', self.uYearIndx)
        self.adjustReproArrays()
        self.getNSessInYear()

    def getNSessInYear(self):
        """
        calc number of session in each year
        """
        self.nSessInYear = np.zeros(len(self.uYearIndx), dtype = int)
        for i in range(len(self.uYearIndx)):
            mask_i = self.yearRecruitIndx == i
            self.nSessInYear[i] = np.sum(mask_i)
#        print('nsessinyear', self.nSessInYear)


    def adjustReproArrays(self):
        """
        ### adjust reproPeriodIndx and reproMask if no recruitment follows (eg Cats)
        """
        fixMask = np.in1d(self.reproPeriodIndx, self.yearRecruitIndx)
        self.reproPeriodIndx[fixMask == 0] = 0
        self.reproMask[fixMask == 0] = 0
#        print('Post Fix self.reproPeriodIndx', self.reproPeriodIndx)
#        print('Post Fix self.reproMask', self.reproMask)
        

    def makePseudoRecruitIndx(self):
        """
        Make indx array for recruitment periods for pseudo sessions        
        """
#        # test elements
#        nDaysInSess = np.zeros(self.nsession, dtype = int)
#        dayCounter = 0

        self.pseudoYearRecruitIndx = np.zeros(self.nPseudoJulYear, dtype = int)
        sessionCounter = 0
        for i in range(1, self.nPseudoJulYear):     
            self.pseudoYearRecruitIndx[i] = self.yearRecruitIndx[sessionCounter]
            pseudJulYr = self.pseudoJulYear[i]
            if(self.startJulSess[sessionCounter] == pseudJulYr):

#                # test element
#                nDaysInSess[sessionCounter] = dayCounter

                sessionCounter += 1
                
#                # test element
#                dayCounter = 0
            
#            # test element
#            dayCounter += 1

            ####### NEW CHANGE  #########
            if sessionCounter == self.nsession:
                sessionCounter = self.nsession - 1
            ############ END    #########

        # days in radians
        self.relWrpCauchy = calcRelWrapCauchy(self.rpara, self.nPseudoJulYear, 
            self.uYearIndx, self.pseudoYearRecruitIndx, self.daypiRecruit)
        self.relWrpCauchy_s = calcRelWrapCauchy(self.rpara_s, self.nPseudoJulYear, 
            self.uYearIndx, self.pseudoYearRecruitIndx, self.daypiRecruit)
        self.getNPseudoSessInYear()
#        calcRelWrapCauchy(self.rpara, self.uYearIndx, daypi, yearRecruitIndx):
#        print('self.pseudoYearRecruitIndx', self.pseudoYearRecruitIndx[0:370])
#        print('self.pseudoYearRecruitIndx End', self.pseudoYearRecruitIndx[-200:])
#        print('self.relWrpCauchy', self.relWrpCauchy[-200:])
#        print('self.pseudoJulYear', self.pseudoJulYear[0:370], len(self.pseudoJulYear))
#        print('self.daypiRecruit', self.daypiRecruit[-200:], len(self.daypiRecruit))
#        print('self.rpara', self.rpara)
#        print('self.uYearIndx', self.uYearIndx)
#        print('unique pseudoyearRecIndx', np.unique(self.pseudoYearRecruitIndx))

#        pseudoReproMask = np.in1d(self.pseudoJulYear, self.params.reproDays)
#        tmpArr = np.zeros((self.nPseudoJulYear, 3))
#        tmpArr[:,0] = self.pseudoJulYear
#        tmpArr[:,1] = self.pseudoYearRecruitIndx
#        tmpArr[:,2] = pseudoReproMask * 1
#        for i in range(self.nPseudoJulYear):
#            print('pseudo tmpArr', tmpArr[i])
#        print('pseudoJulYear  pseudoYrRecIndx pseudoRepMask', tmpArr)
#        print('nsess', self.nsession, 'npseudoJulyear', self.nPseudoJulYear)

#        tmpArr = np.zeros((self.nsession, 2))
#        tmpArr[:,0] = self.uSession
#        tmpArr[:,1] = nDaysInSess
#        print('Sess and nDays in Sess:', tmpArr)



    def makeReproMask(self):
        """
        Make mask for identifying reproductive period
        Use this to get mean pop size to calculate number of recruits
        """
        self.reproMask = np.in1d(self.startJulSess, self.params.reproDays)
        # Make indexing array for periods on which to calc reproductive pop
        indx = 1
        self.reproPeriodIndx = np.zeros(self.nsession, dtype = int)
        for i in range(1, self.nsession):
            if self.reproMask[i]:
                self.reproPeriodIndx[i] = indx
            if self.reproMask[i-1] and not self.reproMask[i]:
                indx += 1
#        print('reproMask', self.reproMask, len(self.reproMask))
#        print('reproPeriodIndx', self.reproPeriodIndx, len(self.reproPeriodIndx))

    def getNPseudoSessInYear(self):
        """
        make arrays of number of pseudo sessions in each recruitment year
        """
        nRecruitYears = len(self.uYearIndx)
        self.nPseudoSessInYear = np.zeros(nRecruitYears, dtype = int)
        for i in range(nRecruitYears):
            tmpArr = self.pseudoYearRecruitIndx[self.pseudoYearRecruitIndx == i]
            self.nPseudoSessInYear[i] = len(tmpArr)
#        print('self.nPseudoSessInYear', self.nPseudoSessInYear)


#    def getInitialN(self):
#        """
#        looping to get reasonable inital N values
#        """
#        self.getInitialRecruits()
#        self.loopGetN()

#    def getInitialRecruits(self):
#        """
#        set up arrays and get recruits for first year
#        """
#        self.N = np.zeros(self.nsession, dtype = np.int)
#        self.reproPop = np.zeros(self.nYear)
#        self.reproPop[0] = self.initialReproPop
#        self.totalRecruits = np.zeros(self.nYear)
#        self.sessionRecruits = np.zeros(self.nsession)
#        nnow = self.initialReproPop

#        self.totalRecruits[0] = (self.reproPop[0] * self.rg) + self.ig
#        yrIndx = self.yearRecruitIndx[0]
#        # numba fx to get recruits for each session in the recruitment year
#        self.recruitYearMask_0 = self.yearRecruitIndx == yrIndx                  # mask of sessions for year
#        self.totalRecruits[0] = (self.initialReproPop * self.rg) + self.ig        # total recruits in year
#        yearRecruits_s = self.proposedRecruitYear(totRecruits_s, recruitYearMask)   # recruits across sessions
#        self.nPseudoSessYear_0 = self.nPseudoSessInYear[yrIndx]
#        self.pseudoRecruitYearMask_0 = self.pseudoYearRecruitIndx == yrIndx
#        relWrpCauchy = self.relWrpCauchy[self.pseudoRecruitYearMask_0]
#        self.pseudoSessRecruitYear_0 = self.totalRecruits[0] * relWrpCauchy
#        # get recruits for all sessions in first recruitment year
#        self.sessionRecruits_0 = self.sessionRecruits[self.recruitYearMask_0]
#        self.sessionJul_0 = self.startJulSess[self.recruitYearMask_0]
#        self.julPseudoYear_0 = self.pseudoJulYear[self.pseudoRecruitYearMask_0] 

#        print('sessJul', self.sessionJul_0)
#        print('pseudoJul', self.julPseudoYear_0)


#        self.sessionRecruits[self.recruitYearMask_0] = getSessRecruitsOneYear((self.nPseudoSessYear_0 - 1), 
#            self.pseudoSessRecruitYear_0, self.sessionRecruits_0, 
#            self.sessionJul_0, self.julPseudoYear_0)
#        self.N[0]= np.random.poisson((nnow + self.sessionRecruits[0]), size = None)


        
#    def loopGetN(self):
#        """
#        # loop thru sessions to get N
#        """
#        yrIndxPast = 0
#        nnow = self.N[0]
#        for i in range(1, self.nsession):
#            yrIndx = self.yearRecruitIndx[i]
#            # if repro period, get repro pop
#            if (self.reproMask[i-1] & (self.reproMask[i] == 0)):
#                yearReproMask = self.reproPeriodIndx == yrIndx
#                rPop = np.sum(self.N[yearReproMask]) / self.nSessInReproPeriod[yrIndx] 
#                self.reproPop[yrIndx] = rPop
#                self.totalRecruits[yrIndx] = (rPop * self.rg) + self.ig        # total recruits in year
#                nPseudoSessYear_i = self.nPseudoSessInYear[yrIndx]
#                pseudoRecruitYearMask = self.pseudoYearRecruitIndx == yrIndx
#                relWrpCauchy = self.relWrpCauchy[pseudoRecruitYearMask]
#                pseudoSessRecruitYear  = self.totalRecruits[yrIndx] * relWrpCauchy
#                self.pseudoSessRecruits[pseudoRecruitYearMask] = pseudoSessRecruitYear
#                # get recruits for all sessions in recruitment year
#                recruitYearMask = self.yearRecruitIndx == yrIndx                  # mask of sessions for year
#                sessionRecruits = self.sessionRecruits[recruitYearMask]
#                sessionJul = self.startJulSess[recruitYearMask]
#                julPseudoYear = self.pseudoJulYear[pseudoRecruitYearMask]
#                # get recruits across all sessions
#                self.sessionRecruits[recruitYearMask] = getSessRecruitsOneYear(nPseudoSessYear_i, pseudoSessRecruitYear,
#                    sessionRecruits, sessionJul, julPseudoYear)
#            # calc nnow
#            nnow = nnow - self.removeDat[i-1] + self.sessionRecruits[i]
#            nnow = np.random.poisson(nnow, size = None)
#            # modify N given conditions
#            if nnow < 1:
#                nnow == self.removeDat[i] + 7
#            if nnow > self.params.maxN:
#                nnow = self.params.maxN -10    #360
#            if nnow <= self.removeDat[i]:
#                nnow = self.removeDat[i] + 15
#            if nnow < 1:
#                nnow = 5
#            self.N[i] = nnow
#        self.reproPop_s = self.reproPop + 1.0
#        self.totalRecruits_s = self.totalRecruits + 1.0
#        self.sessionRecruits_s = self.sessionRecruits.copy()
#        # make pseudo arrays for updating
#        self.pseudoTotRecruitArr = self.totalRecruits[self.pseudoYearRecruitIndx]
#        self.pseudoTotRecruitArr_s = self.totalRecruits_s[self.pseudoYearRecruitIndx]
#        self.pseudoSessRecruits_s = (self.pseudoTotRecruitArr_s *
#            self.relWrpCauchy)

    def initialisePop(self):
        """
        initialise the predator population
        """
        # initial N
        if self.params.species == 'Hedgehogs':
             self.N = (self.removeDat + 20).astype(int)
#            self.N = (np.round(8. * self.removeDat)).astype(int)
        else:
            self.N = (np.round(2. * self.removeDat)).astype(int)
        self.N[self.N < 15] = 15
        # make empty arrays
        self.reproPop = np.zeros(self.nYear)
        self.reproPop[0] = self.initialReproPop
        self.totalRecruits = np.zeros(self.nYear)
        self.totalRecruits[0] = (self.reproPop[0] * self.rg) + self.ig
        self.sessionRecruits = np.zeros(self.nsession)
        # initialise ReproPop and Total recruits
        self.initialiseReproPop()
        # make session Recruits and pseudosession recruits
        self.pseudoTotRecruitArr = self.totalRecruits[self.pseudoYearRecruitIndx]
        self.pseudoSessRecruits = (self.pseudoTotRecruitArr *
            self.relWrpCauchy)
        # get recruits across all sessions
        self.sessionRecruits = getSessRecruitsOneYear(self.nPseudoJulYear,
            self.pseudoSessRecruits, self.sessionRecruits,
            self.startJulSess, self.pseudoJulYear, self.nsession)
        # make copies for proposal arrays
        self.reproPop_s = self.reproPop.copy()
        self.totalRecruits_s = self.totalRecruits.copy()
        self.sessionRecruits_s = self.sessionRecruits.copy()
        self.pseudoTotRecruitArr_s = self.pseudoTotRecruitArr.copy()
        self.pseudoSessRecruits_s = self.pseudoSessRecruits.copy()
        # get first year arrays and recruits
        self.getFirstYearRecruits()

    def initialiseReproPop(self):
        """
        Get reproductive population for all years
        """
        for i in range(1, self.nYear):
            ntmp_s1 = np.sum(self.N[self.reproPeriodIndx == i])
#            print('nsessinrepro', self.nSessInRepro, 'sumN', ntmp_s1)
            nSess = self.nSessInRepro[i]
            self.reproPop[i] = ntmp_s1 / nSess
            self.totalRecruits[i] = (self.reproPop[i] * self.rg) + self.ig
#            print('i', i, 'nsess', nSess, 'sumN', ntmp_s1,
#                'reproPop', self.reproPop[i], 'totRec', self.totalRecruits[i])

#        print('totRec', self.totalRecruits, 'reproPop', self.reproPop)
#        print('reproPeriodIndx', self.reproPeriodIndx)
#        print('N', self.N)
#        print('removeDat', self.removeDat)

    def getFirstYearRecruits(self):
        """
        make arrays and get recruits for first year
        """
        self.nPseudoSessYear_0 = self.nPseudoSessInYear[0]
        self.pseudoRecruitYearMask_0 = self.pseudoYearRecruitIndx == 0
        self.pseudoSessRecruitYear_0 = self.pseudoSessRecruits[self.pseudoRecruitYearMask_0]
        # get recruits for all sessions in first recruitment year
        self.recruitYearMask_0 = self.yearRecruitIndx == 0
        self.sessionRecruits_0 = self.sessionRecruits[self.recruitYearMask_0]
        self.sessionJul_0 = self.startJulSess[self.recruitYearMask_0]
        self.julPseudoYear_0 = self.pseudoJulYear[self.pseudoRecruitYearMask_0] 
        self.nSession_0 = len(self.sessionRecruits_0)


    def getNSessInReproPeriod(self):
        """
        make array of length uYearIndx that has number of sessions
        in each corresponding repro period
        """
        self.nSessInRepro = np.ones(len(self.uYearIndx))
        for i in range(1, len(self.uYearIndx)):
            nsess = len(self.reproPeriodIndx[self.reproPeriodIndx == i])
            self.nSessInRepro[i] = nsess

    def NpredAllSessFX(self, nn):
        """
        calc Npred for all sessions
        """
        Npred = np.zeros(self.nsession)
        Npred[0] = self.reproPop[0] + self.sessionRecruits[0]
        for i in self.uSession[:-1]:
            Nday = nn[i] - self.removeDat[i]
            Nday = np.where(Nday < 0, 0, Nday)
            Nday = Nday + self.sessionRecruits[i+1]        # it[i+1]   # imm upto previous
            Npred[i+1] = Nday
        return(Npred)


    def make_g0_Indx(self):
        """
        make array temporally indexing where high, low and standard g0-multipliers applies
        length is number of session (self.nsession)
        """
        # indx is zero where normal g0 applies
        self.g0_Indx_Multiplier = np.zeros(self.nsession, dtype = int)


        ######### CHANGES HERE  ##########
#       # if ferrets
#       if self.params.species == 'Ferrets':
#           mask1 = ((self.startJulSess >= self.params.g0LowDays[0]) &
#                   (self.startJulSess <= self.params.g0LowDays[1]))
#           mask2 = ((self.startJulSess >= self.params.g0LowDays[0]) &
#                   (self.startJulSess <= self.params.g0LowDays[1))
#       else:
#           # indx is zero where normal g0 applies
#           mask1 = self.startJulSess >= self.params.g0LowDays[0]      
#           mask2 = self.startJulSess <= self.params.g0LowDays[1]      
        ######### END CHANGES   #############
        mask1 = self.startJulSess >= self.params.g0LowDays[0]      
        mask2 = self.startJulSess <= self.params.g0LowDays[1]      
        lowMask = mask1 & mask2
        # indx is 1 when low g0 applies
        self.g0_Indx_Multiplier[lowMask] = 1                    
        # Make high mask
        mask1 = self.startJulSess >= self.params.g0HighDays[0]
        mask2 = self.startJulSess <= self.params.g0HighDays[1]
        highMask = mask1 & mask2
        # indx is 2 when high g0 applies
        self.g0_Indx_Multiplier[highMask] = 2
#        print('self.g0_Indx_Multiplier', self.g0_Indx_Multiplier)


    def gettrapSession(self):
        """
        create session ID for Trap data (nsession * nTrap)
        """
        sess = np.arange(self.nsession)
        self.trapSession = np.repeat(sess, self.nTraps)
#        print("self.trapSession.shape", self.trapSession.shape)

    def getTrapNightsFX(self):
        """
        Get number of trap nights * avail
        Get 0 and 1 array of traps that caught
        """
        self.trapNightsAvail = np.zeros(self.nsession * self.nTraps)
        self.trapTrapped = np.zeros(self.nsession * self.nTraps)            # capt captures by trap and session
        for i in range(self.nsession):
            sessmask = self.trapSession == i                                # sess mask for trap data
            captSeqTrapIDSession = self.captTrapID[self.session == i]    # sequence trap ID in capt6 data
            tmpTN = np.zeros(self.nTraps)                                   # empty tn array to populate
            tmptrapcapt = np.zeros(self.nTraps)                             # length trap dat
            trappedSession = self.trapped[self.session == i]                # length capt dat in sess i
            ndaySess = self.nDays[self.session == i]                        # capt session trap night/day 
            # loop through capt data in session i
            cc = 0
            for j in range(self.nTraps):
                if self.trapID[j] in captSeqTrapIDSession:
#                    if not np.isscalar(ndaySess[captSeqTrapIDSession == self.trapID[j]]):
#                        print('ndaySess', ndaySess[captSeqTrapIDSession == self.trapID[j]])
#                        print('i and j', i, j)

                    tmpTN[j] = ndaySess[captSeqTrapIDSession == self.trapID[j]]     # trap nights in trap j sess i
                    tt_j = trappedSession[captSeqTrapIDSession == self.trapID[j]]   # number of predators trapped
#                    if i == 424:
#                        cc += tt_j
#                        print('tid', self.trapID[j], 'tt_j', tt_j, 'cc', cc)
                    # whether trap captured predator  
                    tmptrapcapt[j] = tt_j                                                 
            # trap nights and captures by trap and session (repeating traps for all session)
            self.trapNightsAvail[sessmask] = tmpTN
            self.trapTrapped[sessmask] = tmptrapcapt
        self.trapNightsAvail_2D = np.expand_dims(self.trapNightsAvail, 1)
#        print('n - rm', self.N - self.removeDat)


class PredatorData(object):
    def __init__(self, basicdata, params, debug = False):
        """
        object to keep latent predator loc, pcaptured, whether trapped, which trap
        """
        # a seris of 1-d arrays maxN * nsession
        predatorSession = np.repeat(np.array(range(basicdata.nsession), dtype= int), basicdata.maxN)
        predatorID = np.tile(np.arange(0, basicdata.maxN), basicdata.nsession)
        predatorPres = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession), dtype=int)
        predatorLoc = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession), dtype=int)
        predatorRemove = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession), dtype=int)
        predatorTrapID = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession), dtype=int)
        predatorPCapt = np.zeros(np.multiply(basicdata.maxN, basicdata.nsession))
        predatorTrapPCapt =  np.zeros(np.multiply(basicdata.maxN, basicdata.nsession))
        # loop through sessions
        for i in range(basicdata.nsession):
            tmpPres = predatorPres[predatorSession ==i]
            tmpPres[0:basicdata.N[i]] = 1
            # array of 0 and 1, 1 where present
            predatorPres[predatorSession == i] = tmpPres
            tmpLoc = predatorLoc[predatorSession == i]
            tmpPCapt = predatorPCapt[predatorSession == i]
            tmpRemove = predatorRemove[predatorSession == i]
            # randomly select predators to remove
            selPredatorRem = np.random.permutation(range(basicdata.N[i]))[0:basicdata.removeDat[i]]            
            tmpRemove[selPredatorRem] = 1
            predatorRemove[predatorSession == i] = tmpRemove
            trapSessMask = basicdata.trapSession == i
            # start to id traps that capture each present predator in session i
            # number trapped by trap in session i
            trappedSession = basicdata.trapTrapped[trapSessMask]

            # number of traps that captured predators
            nTraps = len(trappedSession[trappedSession > 0])
            # array number of captures length of number of traps that captured predators
            selTrapSprungStillOpen = trappedSession[trappedSession > 0]   #np.ones(nTraps)
            # number of predators trapped in all traps in sess i that capt or not 
#            tpredatorcaptsess = basicdata.trapTrapped[basicdata.trapSession == i]
            # array of trap ID that captured predators in sess i
            selTrapIDCaptPredator = basicdata.trapID[trappedSession > 0]             
            # arrays of zeros to populate below            
            tmppredatorTrapID = predatorTrapID[predatorSession == i]                    # arrays of zeros to populate
            tmppredatorTrapPCapt = predatorTrapPCapt[predatorSession == i]
            # trap nights for each trap in session i
            tnightsavailSession = basicdata.trapNightsAvail[trapSessMask]
            # g0 for each trap in session i
            g0Session = basicdata.g0All[basicdata.trapSession == i].flatten()
#            g0Session = basicdata.g0All[trapSessMask]
#            print("g0Sess", g0Session.shape)
            leftToRem = basicdata.removeDat[i]

           # loop through predators present
            for j in range(basicdata.N[i]):
                # assign location using multinomial

                tmpLoc[j] = basicdata.cellID[np.random.multinomial(1, 
                                        basicdata.thMultiNom, size = 1).flatten() == 1]

#                print("tnightsavailSession.shp", tnightsavailSession.shape)
#                print("tmpLoc[j]", tmpLoc[j])
#                print("g0Session.shp", g0Session.shape)
                
                tmpPTrapCapt = initialPPredatorTrapCaptFX(basicdata, 
                    tnightsavailSession , tmpLoc[j], g0Session, debug = False)

                # index of traps with top prob of capt
                topPCaptIndx = np.argsort(tmpPTrapCapt)[-params.nTopTraps:]
                # array of pcapt of only top traps
                topPCapt = tmpPTrapCapt[topPCaptIndx]
                # total prob capture of pred j across all top traps
                tmpPCapt[j] = 1. - np.prod(1. - topPCapt)

                
##########                 # total prob capture of pred j across all traps
##########                tmpPCapt[j] = 1. - np.prod(1. - tmpPTrapCapt)
                if tmpPCapt[j] > 0.97:
                    tmpPCapt[j] = 0.97
                # if predator j is removed then follow:
                if tmpRemove[j] == 1:
                    # ID of traps avail to capture
                    sprungTrapsStillOpen = selTrapIDCaptPredator[selTrapSprungStillOpen > 0]

#                    if i == 424:
#                        print('################################      i and j', i , j)
#                        print('left to remove', leftToRem)
#                        print('sum of captures', np.sum(trappedSession))
#                        print('sprungTrapsStillOpen', sprungTrapsStillOpen)
#                        print('selTrapSprungStillOpen', selTrapSprungStillOpen[selTrapSprungStillOpen > 0])

                    leftToRem = leftToRem - 1
                    # pcapt of traps still open
#                    if i == 424:
#                        print('iiiiiiiii ', i, 'j ', j, )
#                        print('tmpPTrapCapt[sprungTrapsStillOpen]', tmpPTrapCapt[sprungTrapsStillOpen])
                    pCaptAvailTrap = tmpPTrapCapt[sprungTrapsStillOpen]
                    diffMask = pCaptAvailTrap == np.max(pCaptAvailTrap)

                    # trap id with max pcapt captures predator j

#                    print("tmpPTrapCapt.shp", tmpPTrapCapt.shape)
#                    print("pCaptAvailTrap.shp", pCaptAvailTrap.shape)
#                    print("pCaptAvailTrap", pCaptAvailTrap )
#                    print("diffMask", diffMask)
#                    print("sprungTrapsStillOpen.shp", sprungTrapsStillOpen.shape)
#                    print("sprungTrapsStillOpen", sprungTrapsStillOpen)
#                    print("diffmask.shp, i, j", diffMask.shape, i, j)

                    captID = sprungTrapsStillOpen[diffMask]
                    
#                    print("captID.shape", captID.shape)
                    

                    if np.shape(captID)[0] == 1:
                        tmppredatorTrapID[j] = captID
                    else:
                        tmppredatorTrapID[j] = captID[0]

#                    print("tmppredatorTrapID[j]", tmppredatorTrapID[j])
                    
                    # take just 1 
#                    tmppredatorTrapID[j] = captID[0]
                    # prob that the given trap captured predator j for predator data
                    # prob that the given trap captured predator j for predator data
                    tmppredatorTrapPCapt[j] = tmpPTrapCapt[tmppredatorTrapID[j]]
                    # make all probabilities non-zero
                    tmppredatorTrapPCapt[j] = np.where(tmppredatorTrapPCapt[j] < 1.00e-20, 1.00e-20, 
                                                tmppredatorTrapPCapt[j])
                    # reduce availability of that trap to capture other predators j + ...
                    reductionResult = selTrapSprungStillOpen[selTrapIDCaptPredator == tmppredatorTrapID[j]] - 1
                    selTrapSprungStillOpen[selTrapIDCaptPredator == tmppredatorTrapID[j]] = reductionResult

            predatorLoc[predatorSession == i] = tmpLoc
            predatorPCapt[predatorSession == i] = tmpPCapt
            predatorTrapID[predatorSession == i] = tmppredatorTrapID
            predatorTrapPCapt[predatorSession == i] = tmppredatorTrapPCapt

        self.predatorSession = predatorSession
        self.predatorID = predatorID
        self.predatorPres = predatorPres
        self.predatorLoc = predatorLoc
        self.predatorRemove = predatorRemove
        self.predatorTrapID = predatorTrapID
        self.predatorPCapt = predatorPCapt
        self.predatorTrapPCapt = predatorTrapPCapt
        self.predatorPCapt_s = predatorPCapt.copy()
        self.predatorTrapPCapt_s = predatorTrapPCapt.copy()




class CheckingData(object):
    def __init__(self, mcmcobj):
        self.Ngibbs = mcmcobj.Ngibbs
        self.bgibbs = mcmcobj.bgibbs
        self.rgibbs = mcmcobj.rgibbs
        self.igibbs = mcmcobj.igibbs
        self.g0gibbs = mcmcobj.g0gibbs
        self.siggibbs = mcmcobj.siggibbs
        self.rparagibbs = mcmcobj.rparagibbs
        self.g0Multigibbs = mcmcobj.g0Multigibbs
        self.initialRepPopgibbs = mcmcobj.initialRepPopgibbs
        self.startingStep = mcmcobj.startingStep
        self.cc = mcmcobj.cc


class Gibbs(object):
    def __init__(self, mcmcobj, basicdata):
        """
        Pickle structure for mcmc storage arrarys and critical elements
        """
        self.Ngibbs = mcmcobj.Ngibbs
        self.bgibbs = mcmcobj.bgibbs
        self.rgibbs = mcmcobj.rgibbs
        self.igibbs = mcmcobj.igibbs
        self.g0gibbs = mcmcobj.g0gibbs
        self.siggibbs = mcmcobj.siggibbs
        self.rparagibbs = mcmcobj.rparagibbs
        self.g0Multigibbs = mcmcobj.g0Multigibbs
        self.initialRepPopgibbs = mcmcobj.initialRepPopgibbs

        self.nsession = basicdata.nsession
        self.removeDat = basicdata.removeDat
        self.sessionYear = basicdata.sessionYear
        self.sessionMonth = basicdata.sessionMonth
        self.sessionDay = basicdata.sessionDay
        self.trapSession = basicdata.trapSession
        self.trapNightsAvail_2D = basicdata.trapNightsAvail_2D
        self.startJulSess = basicdata.startJulSess
        self.fortnightMask = basicdata.fortnightMask
        self.sessionIntervalMask = basicdata.sessionIntervalMask
        # for calc npred 
        self.uYearIndx = basicdata.uYearIndx
        self.reproPeriodIndx = basicdata.reproPeriodIndx
        self.nYear = basicdata.nYear
        self.daypiRecruit = basicdata.daypiRecruit
        self.yearRecruitIndx = basicdata.yearRecruitIndx
        self.pseudoYearRecruitIndx = basicdata.pseudoYearRecruitIndx
        self.pseudoJulYear = basicdata.pseudoJulYear
        self.nPseudoJulYear = basicdata.nPseudoJulYear
        self.nPseudoSessInYear = basicdata.nPseudoSessInYear

class PredatorTrapData(object):
    def __init__(self, rawdata):
        """
        Pickle structure from manipCats.py 
        """
        # data associated with capture data
        self.week = rawdata.week
        self.captTrapID = rawdata.captTrapID
        self.julianStart = rawdata.julianStart
        self.julianYear = rawdata.julianYear
        self.ttypebait = rawdata.ttypebait
        self.ttype = rawdata.ttype
        self.cattrap = rawdata.cattrap
        self.ferretTrap = rawdata.ferretTrap
        self.hedgehogTrap = rawdata.hedgehogTrap
        self.stoweaTrap = rawdata.stoweaTrap
        self.nDays = rawdata.nDays
        self.year = rawdata.year
        self.fortnightMask = rawdata.fortnightMask
        self.sessionIntervalMask = rawdata.sessionIntervalMask
        # data associated with trap location data
        self.trapX = rawdata.trapX
        self.trapY = rawdata.trapY
        self.trapTrapid = rawdata.trapTrapid
        self.trapBaitIndx = rawdata.trapBaitIndx
        self.nTraps = rawdata.nTraps 
        self.sessionYear = rawdata.sessionYear
        self.sessionMonth = rawdata.sessionMonth
        self.sessionDay = rawdata.sessionDay


"""

class CatParameters():
    def __init__(self, cattmp):
        ## Object to set initial parameters
        
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = cattmp.ngibbs
        self.thinrate = cattmp.thinrate
        self.burnin = cattmp.burnin
        self.interval = cattmp.interval
        self.checkpointfreq = cattmp.checkpointfreq
        self.keepseq = cattmp.keepseq
        self.initialRun = cattmp.initialRun
        self.useCheckedData = cattmp.useCheckedData
        self.species = cattmp.species
        # names
        self.predatorpath = cattmp.predatorpath
        self.outBasicdataFname = cattmp.outBasicdataFname
        self.outPredatordataFname = cattmp.outPredatordataFname
        self.outGibbsFname = cattmp.outGibbsFname
        self.outCheckingFname = cattmp.outCheckingFname
        self.g0_HiLowFname = cattmp.g0_HiLowFname
        self.plotLegendName = cattmp.plotLegendName
        self.removeRecruitPlotFname = cattmpremoveRecruitPlotFname
        self.summaryTableFname = cattmp.summaryTableFname
        self.ParameterFname = cattmp.catParameterFname
        ###############      Initial values parameters for updating
        self.maxN = cattmp.maxN
        self.sigma = cattmp.sigma
        self.sigma_mean = cattmp.sigma_mean
        self.sigma_sd = cattmp.sigma_sd
        self.sigma_search_sd = cattmp.sigma_search_sd
        self.g0_alpha = cattmp.g0_alpha
        self.g0_beta = cattmp.g0_beta
        self.g0Sd = cattmp.g0Sd
        self.g0Multiplier = cattmp.g0Multiplier
        self.g0MultiPrior = cattmp.g0MultiPrior
        self.g0MultiSearch = cattmp.g0MultiSearch
        self.nTopTraps = cattmp.nTopTraps
        self.ig = cattmp.ig
        self.immDistr = cattmp.immDistr
        self.immUniform = cattmp.immUniform
        self.immSearch = cattmp.immSearch
        self.imm_mean = cattmp.imm_mean
        self.imm_sd = cattmp.imm_sd
        self.rg = cattmp.rg
        self.r_shape = cattmp.r_shape
        self.r_scale = cattmp.r_scale
        self.reproSearch = cattmp.reproSearch
        self.initialReproPop = cattmp.initialReproPop
        self.IRR_priors = cattmp.IRR_priors
        self.initialReproSearch = cattmp.initialReproSearch
        self.rpara = cattmp.rpara
        self.mu_wrpc_Priors = cattmp.mu_wrpc_Priors
        self.rho_wrpc_Priors = cattmp.rho_wrpc_Priors
        self.rparaSearch = cattmp.rparaSearch
        self.reproDaysBack = cattmp.reproDaysBack
        self.reproDays = cattmp.reproDays
        self.g0LowDays = cattmp.g0LowDays
        self.g0HighDays = cattmp.g0HighDays
        # modify  variables used for habitat model
        self.xdatDictionary = cattmp.xdatDictionary
        self.scaleEast = cattmp.scaleEast
        self.scaleNorth = cattmp.scaleNorth
        self.scaleTuss = cattmp.scaleTuss
        self.scaleCatFerret = cattmp.scaleCatFerret
        self.xdatIndx = cattmp.xdatIndx    
        self.b = cattmp.b
        self.bPrior = cattmp.bPrior
        self.bPriorSD = cattmp.bPriorSD
        self.nbcov = cattmp.nbcov
        ####################################
        ####################################    END CATS
        ####################################






class FerretParameters():
    def __init__(self, ferrettmp):
        
        ## Object to set initial parameters
        
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = ferrettmp.ngibbs
        self.thinrate = ferrettmp.thinrate
        self.burnin = ferrettmp.burnin
        self.interval = ferrettmp.interval
        self.checkpointfreq = ferrettmp.checkpointfreq
        self.keepseq = ferrettmp.keepseq
        self.initialRun = ferrettmp.initialRun
        self.useCheckedData = ferrettmp.useCheckedData
        self.species = ferrettmp.species
        # names
        self.predatorpath = ferrettmp.predatorpath
        self.outBasicdataFname = ferrettmp.outBasicdataFname
        self.outPredatordataFname = ferrettmp.outPredatordataFname
        self.outGibbsFname = ferrettmp.outGibbsFname
        self.outCheckingFname = ferrettmp.outCheckingFname
        self.g0_HiLowFname = ferrettmp.g0_HiLowFname
        self.plotLegendName = ferrettmp.plotLegendName
        self.removeRecruitPlotFname = ferrettmpremoveRecruitPlotFname
        self.summaryTableFname = ferrettmp.summaryTableFname
        self.ParameterFname = ferrettmp.ferretParameterFname
        ###############      Initial values parameters for updating
        self.maxN = ferrettmp.maxN
        self.sigma = ferrettmp.sigma
        self.sigma_mean = ferrettmp.sigma_mean
        self.sigma_sd = ferrettmp.sigma_sd
        self.sigma_search_sd = ferrettmp.sigma_search_sd
        self.g0_alpha = ferrettmp.g0_alpha
        self.g0_beta = ferrettmp.g0_beta
        self.g0Sd = ferrettmp.g0Sd
        self.g0Multiplier = ferrettmp.g0Multiplier
        self.g0MultiPrior = ferrettmp.g0MultiPrior
        self.g0MultiSearch = ferrettmp.g0MultiSearch
        self.nTopTraps = ferrettmp.nTopTraps
        self.ig = ferrettmp.ig
        self.immDistr = ferrettmp.immDistr
        self.immUniform = ferrettmp.immUniform
        self.immSearch = ferrettmp.immSearch
        self.imm_mean = ferrettmp.imm_mean
        self.imm_sd = ferrettmp.imm_sd
        self.rg = ferrettmp.rg
        self.r_shape = ferrettmp.r_shape
        self.r_scale = ferrettmp.r_scale
        self.reproSearch = ferrettmp.reproSearch
        self.initialReproPop = ferrettmp.initialReproPop
        self.IRR_priors = ferrettmp.IRR_priors
        self.initialReproSearch = ferrettmp.initialReproSearch
        self.rpara = ferrettmp.rpara
        self.mu_wrpc_Priors = ferrettmp.mu_wrpc_Priors
        self.rho_wrpc_Priors = ferrettmp.rho_wrpc_Priors
        self.rparaSearch = ferrettmp.rparaSearch
        self.reproDaysBack = ferrettmp.reproDaysBack
        self.reproDays = ferrettmp.reproDays
        self.g0LowDays = ferrettmp.g0LowDays
        self.g0HighDays = ferrettmp.g0HighDays
        # modify  variables used for habitat model
        self.xdatDictionary = ferrettmp.xdatDictionary
        self.scaleEast = ferrettmp.scaleEast
        self.scaleNorth = ferrettmp.scaleNorth
        self.scaleTuss = ferrettmp.scaleTuss
        self.scaleCatFerret = ferrettmp.scaleCatFerret
        self.xdatIndx = ferrettmp.xdatIndx    
        self.b = ferrettmp.b
        self.bPrior = ferrettmp.bPrior
        self.bPriorSD = ferrettmp.bPriorSD
        self.nbcov = ferrettmp.nbcov
        ####################################
        ####################################    END FERRETS
        ####################################





class HedgehogParameters():
    def __init__(self, hedgehogtmp):
        
        ## Object to set initial parameters
        
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = hedgehogtmp.ngibbs
        self.thinrate = hedgehogtmp.thinrate
        self.burnin = hedgehogtmp.burnin
        self.interval = hedgehogtmp.interval
        self.checkpointfreq = hedgehogtmp.checkpointfreq
        self.keepseq = hedgehogtmp.keepseq
        self.initialRun = hedgehogtmp.initialRun
        self.useCheckedData = hedgehogtmp.useCheckedData
        self.species = hedgehogtmp.species
        # names
        self.predatorpath = hedgehogtmp.predatorpath
        self.outBasicdataFname = hedgehogtmp.outBasicdataFname
        self.outPredatordataFname = hedgehogtmp.outPredatordataFname
        self.outGibbsFname = hedgehogtmp.outGibbsFname
        self.outCheckingFname = hedgehogtmp.outCheckingFname
        self.g0_HiLowFname = hedgehogtmp.g0_HiLowFname
        self.plotLegendName = hedgehogtmp.plotLegendName
        self.removeRecruitPlotFname = hedgehogtmpremoveRecruitPlotFname
        self.summaryTableFname = hedgehogtmp.summaryTableFname
        self.ParameterFname = hedgehogtmp.hedgehogParameterFname
        ###############      Initial values parameters for updating
        self.maxN = hedgehogtmp.maxN
        self.sigma = hedgehogtmp.sigma
        self.sigma_mean = hedgehogtmp.sigma_mean
        self.sigma_sd = hedgehogtmp.sigma_sd
        self.sigma_search_sd = hedgehogtmp.sigma_search_sd
        self.g0_alpha = hedgehogtmp.g0_alpha
        self.g0_beta = hedgehogtmp.g0_beta
        self.g0Sd = hedgehogtmp.g0Sd
        self.g0Multiplier = hedgehogtmp.g0Multiplier
        self.g0MultiPrior = hedgehogtmp.g0MultiPrior
        self.g0MultiSearch = hedgehogtmp.g0MultiSearch
        self.nTopTraps = hedgehogtmp.nTopTraps
        self.ig = hedgehogtmp.ig
        self.immDistr = hedgehogtmp.immDistr
        self.immUniform = hedgehogtmp.immUniform
        self.immSearch = hedgehogtmp.immSearch
        self.imm_mean = hedgehogtmp.imm_mean
        self.imm_sd = hedgehogtmp.imm_sd
        self.rg = hedgehogtmp.rg
        self.r_shape = hedgehogtmp.r_shape
        self.r_scale = hedgehogtmp.r_scale
        self.reproSearch = hedgehogtmp.reproSearch
        self.initialReproPop = hedgehogtmp.initialReproPop
        self.IRR_priors = hedgehogtmp.IRR_priors
        self.initialReproSearch = hedgehogtmp.initialReproSearch
        self.rpara = hedgehogtmp.rpara
        self.mu_wrpc_Priors = hedgehogtmp.mu_wrpc_Priors
        self.rho_wrpc_Priors = hedgehogtmp.rho_wrpc_Priors
        self.rparaSearch = hedgehogtmp.rparaSearch
        self.reproDaysBack = hedgehogtmp.reproDaysBack
        self.reproDays = hedgehogtmp.reproDays
        self.g0LowDays = hedgehogtmp.g0LowDays
        self.g0HighDays = hedgehogtmp.g0HighDays
        # modify  variables used for habitat model
        self.xdatDictionary = hedgehogtmp.xdatDictionary
        self.scaleEast = hedgehogtmp.scaleEast
        self.scaleNorth = hedgehogtmp.scaleNorth
        self.scaleTuss = hedgehogtmp.scaleTuss
        self.scaleCatFerret = hedgehogtmp.scaleCatFerret
        self.xdatIndx = hedgehogtmp.xdatIndx    
        self.b = hedgehogtmp.b
        self.bPrior = hedgehogtmp.bPrior
        self.bPriorSD = hedgehogtmp.bPriorSD
        self.nbcov = hedgehogtmp.nbcov
        ####################################
        ####################################    END HEDGEHOGS
        ####################################




class StoweaParameters():
    def __init__(self, stoweatmp):
        
        ## Object to set initial parameters
        
        # Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = stoweatmp.ngibbs
        self.thinrate = stoweatmp.thinrate
        self.burnin = stoweatmp.burnin
        self.interval = stoweatmp.interval
        self.checkpointfreq = stoweatmp.checkpointfreq
        self.keepseq = stoweatmp.keepseq
        self.initialRun = stoweatmp.initialRun
        self.useCheckedData = stoweatmp.useCheckedData
        self.species = stoweatmp.species
        # names
        self.predatorpath = stoweatmp.predatorpath
        self.outBasicdataFname = stoweatmp.outBasicdataFname
        self.outPredatordataFname = stoweatmp.outPredatordataFname
        self.outGibbsFname = stoweatmp.outGibbsFname
        self.outCheckingFname = stoweatmp.outCheckingFname
        self.g0_HiLowFname = stoweatmp.g0_HiLowFname
        self.plotLegendName = stoweatmp.plotLegendName
        self.removeRecruitPlotFname = stoweatmpremoveRecruitPlotFname
        self.summaryTableFname = stoweatmp.summaryTableFname
        self.ParameterFname = stoweatmp.stoweaParameterFname
        ###############      Initial values parameters for updating
        self.maxN = stoweatmp.maxN
        self.sigma = stoweatmp.sigma
        self.sigma_mean = stoweatmp.sigma_mean
        self.sigma_sd = stoweatmp.sigma_sd
        self.sigma_search_sd = stoweatmp.sigma_search_sd
        self.g0_alpha = stoweatmp.g0_alpha
        self.g0_beta = stoweatmp.g0_beta
        self.g0Sd = stoweatmp.g0Sd
        self.g0Multiplier = stoweatmp.g0Multiplier
        self.g0MultiPrior = stoweatmp.g0MultiPrior
        self.g0MultiSearch = stoweatmp.g0MultiSearch
        self.nTopTraps = stoweatmp.nTopTraps
        self.ig = stoweatmp.ig
        self.immDistr = stoweatmp.immDistr
        self.immUniform = stoweatmp.immUniform
        self.immSearch = stoweatmp.immSearch
        self.imm_mean = stoweatmp.imm_mean
        self.imm_sd = stoweatmp.imm_sd
        self.rg = stoweatmp.rg
        self.r_shape = stoweatmp.r_shape
        self.r_scale = stoweatmp.r_scale
        self.reproSearch = stoweatmp.reproSearch
        self.initialReproPop = stoweatmp.initialReproPop
        self.IRR_priors = stoweatmp.IRR_priors
        self.initialReproSearch = stoweatmp.initialReproSearch
        self.rpara = stoweatmp.rpara
        self.mu_wrpc_Priors = stoweatmp.mu_wrpc_Priors
        self.rho_wrpc_Priors = stoweatmp.rho_wrpc_Priors
        self.rparaSearch = stoweatmp.rparaSearch
        self.reproDaysBack = stoweatmp.reproDaysBack
        self.reproDays = stoweatmp.reproDays
        self.g0LowDays = stoweatmp.g0LowDays
        self.g0HighDays = stoweatmp.g0HighDays
        # modify  variables used for habitat model
        self.xdatDictionary = stoweatmp.xdatDictionary
        self.scaleEast = stoweatmp.scaleEast
        self.scaleNorth = stoweatmp.scaleNorth
        self.scaleTuss = stoweatmp.scaleTuss
        self.scaleCatFerret = stoweatmp.scaleCatFerret
        self.xdatIndx = stoweatmp.xdatIndx    
        self.b = stoweatmp.b
        self.bPrior = stoweatmp.bPrior
        self.bPriorSD = stoweatmp.bPriorSD
        self.nbcov = stoweatmp.nbcov
        ####################################
        ####################################    END STOATS AND WEASELS
        ####################################


"""