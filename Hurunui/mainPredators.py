#!/usr/bin/env python

import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
from numba import jit
import pickle
import datetime
import basicsModule
import empiricalPredator



########            Main function
#######
def main(params):

    print('########################')
    print('########################')
    print('###')
    print('#    Species = ', params.species)
    print('###')
    print('########################')
    print('########################')
    print('Initial run: ', params.initialRun)
    print('Use checked data: ', params.useCheckedData)
    print('Model ID: ', params.modelID)
    # paths and data to read in
    if params.species == 'Hedgehogs':
        covDatFile = os.path.join(params.predatorpath,'tussockXY50m.csv')
    else:
        covDatFile = os.path.join(params.predatorpath,'covDat250.csv')
    #trapDatFile = os.path.join(predatorpath,'trapDat3.csv')

    # initiate basicdata class and object when do not read in previous results
    if params.initialRun:
        # read in the pickled capt and trap data from 'manipCats.py'
        predatorTrapFile = os.path.join(params.predatorpath,'out_manipdata2.pkl')
        fileobj = open(predatorTrapFile, 'rb')
        predatortrapdata = pickle.load(fileobj)
        fileobj.close()
        # initiate basicdata from script
        basicdata = basicsModule.BasicData(predatortrapdata, covDatFile, params)
        # initiate predatordata class
        predatordata = basicsModule.PredatorData(basicdata, params)

    # read in pickled results (basicdata and predatordata) from a previous run
    else:
        inputBasicdata = os.path.join(params.predatorpath, params.outBasicdataFname)
        fileobj = open(inputBasicdata, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()
        print('outBasicdataFname: ', params.outBasicdataFname)
        # read in pickled predatordata class
        inputPreddata = os.path.join(params.predatorpath, params.outPredatordataFname)
        fileobj = open(inputPreddata, 'rb')
        predatordata = pickle.load(fileobj)
        fileobj.close()
        print('outPredatordataFname: ', params.outPredatordataFname)


    ## run mcmc 
    if params.useCheckedData:
        inputCheckingData = os.path.join(params.predatorpath, params.outCheckingFname)
        fileobj = open(inputCheckingData, 'rb')
        checkingdata = pickle.load(fileobj)
        fileobj.close()
        print('outCheckingFname: ', params.outCheckingFname)
        mcmcobj = empiricalPredator.MCMC(params, predatordata, basicdata, checkingdata)
    else:
        mcmcobj = empiricalPredator.MCMC(params, predatordata, basicdata)


    ## SAVE PICKLES
    # pickle basic data from present run to be used to initiate new runs
    #    outBasicdata = os.path.join(predatorpath,'out_basicdata.pkl')
    fileobj = open(params.outBasicdataFname, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    # pickle predator data from present run to be used to initiate new runs
    #    outPredatordata = os.path.join(predatorpath,'out_predatordata.pkl')
    fileobj = open(params.outPredatordataFname, 'wb')
    pickle.dump(predatordata, fileobj)
    fileobj.close()


    # pickle mcmc results for post processing
    #    outGibbs = os.path.join(predatorpath,'out_gibbs.pkl')
    gibbsobj = basicsModule.Gibbs(mcmcobj, basicdata)
    fileobj = open(params.outGibbsFname, 'wb')
    pickle.dump(gibbsobj, fileobj)
    fileobj.close()


    # Save a checkpoint, but not if this is the first or the last step
    # include simplified MCMC data
    if (( (mcmcobj.g % params.checkpointfreq == 0) or (mcmcobj.g == (mcmcobj.stoppingStep - 1))) 
            and (mcmcobj.g != mcmcobj.startingStep) and (mcmcobj.g != (mcmcobj.maxsteps - 1)) ):
        mcmcobj.startingStep = mcmcobj.g + 1
        print('writeChecking = True')

        # Write the checking data to a separate PKL
        checkingdata = basicsModule.CheckingData(mcmcobj)
        fileobj = open(params.outCheckingFname, 'wb')
        pickle.dump(checkingdata, fileobj)      
        fileobj.close()




if __name__ == '__main__':
    main()

