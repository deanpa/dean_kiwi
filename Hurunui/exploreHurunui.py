#!/usr/bin/env python

import os
#from scipy import stats
import numpy as np
#from numba import jit
import pickle
import pylab as P
#from scipy.stats.mstats import mquantiles
#import prettytable
from datetime import datetime 
#import calendar
from dateutil.relativedelta import relativedelta

def logit(x):
    """
    logit function
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    inverse logit function
    """
    return np.exp(x) / (1 + np.exp(x))


def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
#    strArray = np.empty(nArray, dtype = 'U8')
    for i in range(1, nArray):
#    for i in range(nArray):
        strArr_i = str(byteArray[i], 'utf-8')
#        strArray[i] = strArr_i
        strArray = np.append(strArray, strArr_i)
    return(strArray)


def formatDate(inArray):
    n = len(inArray)
    outArray = np.empty(n, dtype = datetime)
    for i in range(n):
        date_i = inArray[i]
        if date_i != '':
            outArray[i] = datetime.strptime(date_i, '%d/%m/%Y').date()
        else:
            outArray[i] = datetime(2010, 4, 2).date()
    return(outArray)


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D

#def add_months(sourcedate, months):
#    month = sourcedate.month - 1 + months
#    year = sourcedate.year + month // 12
#    month = month % 12 + 1
#    day = min(sourcedate.day, calendar.monthrange(year,month)[1])
#    return datetime.date(year, month, day)

class Params(object):
    def __init__(self):
        """
        ## SET MODEL PARAMS
        """
        self.sigma = 325.0
        self.scenario = 1
        self.N0 = 7
        self.pRem = .80
        self.WC = np.array([1.65, 1.49])
        self.annRecruits =60
        self.lambdaPara = 1.4
        self.simNYears = 5 

        self.target = 30
        

        




        print('Scenario:', self.scenario)

class ExploreDat(object):
    def __init__(self, trapDatFile, params):
        """
        Object explore trap data
        """
        self.params = params
        ############## Run functions
        self.readTrapDat(trapDatFile)
        self.getDates()
#        self.monthsTraps()

#        self.getTrapData_Sim()
#        self.makeAreaRasters()
#        self.getTrapsPerHa()



        self.getDateRemovalHurunui()


        self.simStoat()
        ############## Finish running functions


    def readTrapDat(self, trapDatFile):
        """
        read in trap data
        """
        self.trapDat = np.genfromtxt(trapDatFile,  delimiter=',', names=True,
            dtype=['S32', 'S10', 'f8', 'f8', 'S10', 'S10', 'S10', 'S10', 'i8', 'S10',
            'S32', 'S32', 'S32', 'i8', 'S10', 'i8', 'S10'])

        self.x = self.trapDat['easting']
        self.y = self.trapDat['northing']
        self.nDat = len(self.y)
        self.trapID = decodeBytes(self.trapDat['deviceID'], self.nDat)
        self.singleDouble = decodeBytes(self.trapDat['singleDouble'], self.nDat)
        self.deviceType = decodeBytes(self.trapDat['deviceType'], self.nDat)
        self.status = decodeBytes(self.trapDat['operationalStatus'], self.nDat)
        self.lure = decodeBytes(self.trapDat['lure'], self.nDat)
        self.result1 = decodeBytes(self.trapDat['result1'], self.nDat)
        self.result2 = decodeBytes(self.trapDat['result2'], self.nDat)
        dateString = decodeBytes(self.trapDat['dateDeploy'], self.nDat)
        self.date = formatDate(dateString)
        self.nights = self.trapDat['nights']

        self.monthsKeep = np.array([2016, 2014, 2015, 2014, 2015, 2016, 0, 2010,
            2016, 2016, 2015, 2012])
        self.scenarioMonths = [[1, 2, 11, 12], [1,2,4,9,11,12], [1,2,4,6, 8,9,11,12]]

    def getDates(self):
        """
        get dates, months, days, years, julian and julianYear
        """
#        self.date = np.empty(self.nDat, dtype = datetime)
        self.month = np.zeros(self.nDat, dtype = int)
        self.day = np.zeros(self.nDat, dtype = int)
        self.year = np.zeros(self.nDat, dtype = int)
        self.jul = np.zeros(self.nDat, dtype = int)
        self.julYear = np.zeros(self.nDat, dtype = int)
        self.startDate = np.min(self.date)
        print('start date', self.startDate)

        for i in range(self.nDat):
            date_i = self.date[i]
            diffDay = date_i - self.startDate
            self.jul[i] = diffDay.days
            d_Tup = date_i.timetuple()
            self.julYear[i] = d_Tup.tm_yday
            self.year[i] = d_Tup.tm_year
            self.month[i] = d_Tup.tm_mon
        self.uYears = np.unique(self.year)
        self.nYears = len(self.uYears)
        
    def monthsTraps(self):
        """
        view number days and traps in each month
        """
        for i in range(self.nYears):
            yearMask = self.year == self.uYears[i]
            uMon_i = np.unique(self.month[yearMask])
            for j in range(len(uMon_i)):
                monthMask = self.month == uMon_i[j]
                yearMonthMask = yearMask & monthMask
                uDays = np.unique(self.julYear[yearMonthMask])
                uTraps = np.unique(self.trapID[yearMonthMask])
                print('yr', self.uYears[i], 'mon', uMon_i[j], 'days', uDays,
                    'n traps', len(uTraps))


    def getTrapData_Sim(self):
        #self.monthsKeep = np.array([2016, 2014, 2015, 2014, 2015, 2016, 0, 2010,
        #    2016, 2016, 2015, 2012])
        #self.scenarioMonths = [[1, 2, 11, 12], [1,2,4,9,11,12], [1,2,4,6, 8,9,11,12]]        
        monthSim = self.scenarioMonths[self.params.scenario - 1]
        print('monthSim', monthSim)
        self.simTrapID  = [] 
        self.simYear = []
        self.simMonth = []
        self.simX = []
        self.simY = []
        for i in range(len(monthSim)):
            month_indx = monthSim[i] - 1
            monthMask = self.month == monthSim[i]
            yearMask = self.year == self.monthsKeep[month_indx]
            yearMonthMask = monthMask & yearMask
            totalTrapID = self.trapID[yearMonthMask]
            uTrapID = np.unique(totalTrapID)
            nUTrapID = len(uTrapID)
            for j in range(nUTrapID):
                tid = uTrapID[j]
                trapMask = self.trapID == tid
                x_ij = self.x[trapMask][0]
                if np.isnan(x_ij):
                    continue

                self.simTrapID.append(tid)
                self.simYear.append(self.monthsKeep[month_indx])
                self.simMonth.append(monthSim[i])
                y = self.y[trapMask][0]
                self.simX.append(x_ij)
                self.simY.append(y)
#                if i == 0:
#                    print('mon', monthSim[i], 'year', self.monthsKeep[month_indx], 
#                        'tid', tid, 'x', x_ij, 'y', y) 
        self.simTrapID = np.array(self.simTrapID)
        self.simYear = np.array(self.simYear)
        self.simMonth = np.array(self.simMonth)
        self.simX = np.array(self.simX)
        self.simY = np.array(self.simY)
        print('ntraps in simTrapID', len(self.simTrapID))

    def makeAreaRasters(self):
        """
        ## FOR RASTER CELLS CALC DISTANCE TO NEAREST TRAP
        """
        ## self.maxSigma = 340
        self.areaDistances = []
        self.cellX = []
        self.cellY = []
        uTrapID = np.unique(self.simTrapID)
        self.cellX = []
        self.cellY = []
        nUTrap = len(uTrapID)
        x = np.zeros(nUTrap)
        y = np.zeros(nUTrap)
        print('n', nUTrap)
        xyMask = self.simTrapID == uTrapID[0]
        print('len mask', len(xyMask))
        for i in range(nUTrap):
            xyMask = self.simTrapID == uTrapID[i]
            x[i] = self.simX[xyMask][0]
            y[i] = self.simY[xyMask][0]
        ## trap info in site j
        ulx = np.nanmin(x) - (2.0 * 4.0 * self.params.sigma)
        uly = np.nanmax(y) + (2.0 * 4.0 * self.params.sigma)
        lrx = np.nanmax(x) + (2.0 * 4.0 * self.params.sigma)
        lry = np.nanmin(y) - (2.0 * 4.0 * self.params.sigma)
        area = (lrx - ulx) * (uly - lry) 
        print('Rectangular area', area / 1E4)
        ## LOOP THRU ALL CELLS TO GET DISTANCE
        y_j = uly
        area_i_Dist = []
        cc = 0
        while y_j >= lry:
            x_j = ulx
            while x_j <= lrx:
                d_ij = distFX(x_j, y_j, x, y)
                minDist = np.min(d_ij)
                self.areaDistances.append(minDist)
#                if cc < 20:
#                    print('cc', cc, 'x', x_j, 'lrx', lrx, 'y_j', y_j, 
#                        'lry', lry, 'minD', minDist, 'area', self.areaDistances)
                self.cellX.append(x_j)
                self.cellY.append(y_j)
                x_j += 100.0
            y_j -= 100.0
            cc += 1
        
        self.areaDistances = np.array(self.areaDistances)
        trapCellDistMask = self.areaDistances <= 71.0     #(2.45 * self.params.sigma))
        conservationMask = (self.areaDistances <= (2.45 * self.params.sigma))
        self.cellX = np.array(self.cellX)[trapCellDistMask]
        self.cellY = np.array(self.cellY)[trapCellDistMask]
        self.trappingArea = np.sum(trapCellDistMask) 
#        print('trappin area in ha', self.trappingArea)
        print('Conservation Area', np.sum(conservationMask))       


    def getTrapsPerHa(self):
        """
        traps per ha
        """
        cellCount = np.zeros(len(self.cellX))
        self.uTrapID = np.unique(self.simTrapID)
        self.nUTrapID = len(self.uTrapID)
        self.x = np.zeros(self.nUTrapID)
        self.y = np.zeros(self.nUTrapID)
        print('n unique traps', self.nUTrapID)
#        xyMask = self.simTrapID == self.uTrapID[0]
        for i in range(self.nUTrapID):
            xyMask = self.simTrapID == self.uTrapID[i]
            self.x[i] = self.simX[xyMask][0]
            self.y[i] = self.simY[xyMask][0]
            dist = distFX(self.x[i], self.y[i], self.cellX, self.cellY)
            minDist = np.min(dist)
            minMask = dist == minDist
            cellCount[minMask] += 1
        nCellsWithTraps = np.sum(cellCount > 0)
        print('n cells with traps', nCellsWithTraps)


    def getDateRemovalHurunui(self):
        self.plotDates = []
        d_i = self.startDate + relativedelta(day=31)
        self.plotDates.append(d_i)
        self.maxDate = np.max(self.date) + relativedelta(day=31)
        while d_i < self.maxDate:
            d_i = d_i + relativedelta(months =+1)
            d_i = d_i + relativedelta(day=31)
            self.plotDates.append(d_i)
#            print('dates', d_i)
        self.plotDates = np.array(self.plotDates)
        self.nMonths = len(self.plotDates)
        self.stoatRem = np.zeros(self.nMonths)
        self.simMonth = np.zeros(self.nMonths)
        self.simYear = np.zeros(self.nMonths)
        for i in range(self.nMonths):
            mon_i = self.plotDates[i].month
            yr_i = self.plotDates[i].year
            monMask = self.month == mon_i
            yrMask = self.year == yr_i
            monYearMask = yrMask & monMask
            if np.sum(monYearMask) == 0:
                self.stoatRem[i] = np.nan
            else:
                nStoat1 = np.sum(self.result1[monYearMask] == 'stoat')
                nStoat2 = np.sum(self.result2[monYearMask] == 'stoat')            
                self.stoatRem[i] = nStoat1 + nStoat2
#            print('mon', mon_i, 'yr', yr_i, 'Date', self.plotDates[i],
#                'stoatRem', self.stoatRem[i])
        P.figure(figsize=(11,9))
        P.plot(self.plotDates, self.stoatRem, 'ko')
        P.plot(self.plotDates, self.stoatRem, color = 'k')
        P.xlabel('Dates', fontsize = 17)
        P.ylabel('Stoats removed', fontsize = 17)
        P.show()



    def simStoat(self):
        self.simYears = np.repeat(np.arange(1, self.params.simNYears + 1), 12)
        monSeq = np.array([12, 1,2,3,4,5,6,7,8,9,10,11])
        self.simMonths = np.tile(monSeq, self.params.simNYears)
        self.simNMonths = len(self.simMonths)
        self.seqMonths = np.arange(self.simNMonths, dtype = int)
        self.recruitIndx = np.repeat(np.arange(self.params.simNYears),
            12)
        self.daypi = monSeq / 12 * 2.0 * np.pi
        self.recruits = np.zeros(self.simNMonths)
        self.nRemoved = np.zeros(self.simNMonths)
        self.nBeforeTrap = np.zeros(self.simNMonths)
        self.getRelWrapCauchy()
#        recruitMask = self.recruitIndx == 0
#        self.recruits[recruitMask] = np.random.poisson(self.params.annRecruits * 
#                                    self.relWrpCauch)
        N_i = self.params.N0
        self.N = np.zeros(self.simNMonths)
        print('rec indx', self.recruitIndx, 'simMon', self.simMonths, 
                'simyear', self.simYears)
        print('len recindx', len(self.recruitIndx), 'simmonth', self.simMonths)

        startYear = self.startDate.year
        date_i = datetime(2020,11,30).date() 
        self.simDates = []        
        for i in range(self.simNMonths):
            date_i = date_i + relativedelta(months=+1)
            date_i = date_i + relativedelta(day=31)
            self.simDates.append(date_i)
            if self.simMonths[i] == 12:
                reproRec = N_i * self.params.lambdaPara
                recMask = self.recruitIndx == self.recruitIndx[i]
                self.recruits[recMask] = ((self.params.annRecruits + reproRec) * 
                                    self.relWrpCauch)
            rec_i = self.recruits[i]
            N_i = N_i + rec_i
            self.nBeforeTrap[i] = N_i
            nRem = 0
            if np.in1d(self.simMonths[i], self.scenarioMonths[self.params.scenario - 1]):
                nRem = np.random.binomial(N_i, self.params.pRem)
                self.nRemoved[i] = nRem
            else:
                self.nRemoved[i] = np.nan
            meanPop = N_i - nRem
#            self.N[i] = 0
#            if ~ np.isnan(meanPop):
            self.N[i] = np.random.poisson(meanPop)
            
            print('date', self.simDates[i], 'yr', self.simYears[i], 'mon', self.simMonths[i], 'N_i', N_i,
                'rec_i', rec_i, 'nRem', nRem, 'N', self.N[i])
            N_i = self.N[i]
        self.simDates = np.array(self.simDates)
        P.figure(figsize = (13,10))
        ax = P.gca()

        lns1 = ax.plot(self.simDates, self.nBeforeTrap, color = 'k', linewidth = 3,
                label='Stoats before trapping')
        lns2 = ax.plot(self.simDates, self.recruits, color = 'b', linewidth = 3,
                label = 'Recruits')
        lns3 = ax.plot(self.simDates, self.nRemoved, 'rd', label = 'Removed')
        P.axhline(y=self.params.target, color = 'k', linestyle = '--')
        lns = lns1 + lns2 + lns3
        labs = [l.get_label() for l in lns]
        ax.legend(lns, labs, loc = 'upper left')
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax.set_xlabel('Dates', fontsize = 17)
        ax.set_ylabel('Stoats', fontsize = 17)
        P.savefig('simScenario' + str(self.params.scenario) + '.png', format='png')

        P.show()

#        self.N0 = 22
#        self.pRem = .3
#        self.WC = np.array([1.65, 1.49])
#        self.annRecruit = 100 
        
    def getRelWrapCauchy(self):
        rho = self.params.WC[1]
        mu = self.params.WC[0]
        sinh_rho = np.sinh(rho)
        cosh_rho = np.cosh(rho)
        cos_mu_th = np.cos(self.daypi - mu)
        dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
        self.relWrpCauch = dwrpc / np.sum(dwrpc)     #  dc.sum()


    def getTrapsPerHa(self):
        """
        get number of traps per ha
        """
        x = 1 









########            Main function
#######
def main():

    dp = os.getenv('KIWIPROJDIR')
    if dp is not None:
        stoatpath = os.getenv('KIWIPROJDIR') + '/Hurunui'
    else:
        stoatpath = '.' 
    # paths and data to read in
    trapDatFile = os.path.join(stoatpath, 'Data', 'SB_data_DeanManip.csv')
    
    params = Params()

    exploredat = ExploreDat(trapDatFile, params)

  
#    inputGibbs = os.path.join(predatorpath, 'out_gibbs.pkl')
#    fileobj = open(inputGibbs, 'rb')
#    gibbsobj = pickle.load(fileobj)
#    fileobj.close()


if __name__ == '__main__':
    main()



