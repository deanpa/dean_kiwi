#!/usr/bin/env python

import numpy as np
#from scipy import stats
import pylab as P
from scipy.stats.mstats import mquantiles

def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D




class PDetect():
    def __init__(self):
#        self.g0Range = [0.01, 0.15]
#        self.sigmaRange = [30.0, 200.0]
#        self.n = 1
        self.iter = 1000
        self.lineSpace = 300.0
        self.ttSpace = 50.0
        nTT_Line = 10.0
        nLines = 12.0
        self.xMaxTrap = ((nLines - 1.0) * self.lineSpace)
        self.yMaxTrap = ((nTT_Line - 1.0) * self.ttSpace)
        self.xRange = [0.0, (self.xMaxTrap + self.lineSpace)]
        self.yRange = [-self.ttSpace, (self.ttSpace * (nTT_Line - 1.0))]
#        print('xMaxTrap', self.xMaxTrap, 'yMaxTrap', self.yMaxTrap)

        self.g0 = 0.05
        self.sigma = 20.0
        

        ####
        # run functions
        self.makeLocations()


        # end functions
        ####

    def makeLocations(self):
        yArr = np.arange(self.yRange[1], self.yRange[0], -self.ttSpace)
        xArr = np.arange(self.xRange[0], self.xRange[1], self.lineSpace)

        self.xTrap = np.tile(xArr, len(yArr))
        self.yTrap = np.repeat(yArr, len(xArr))

#        print('xTrap', self.xTrap[:20], 'yTrap', self.yTrap[:20])

        self.randX = np.random.uniform((-4.0*self.sigma), (self.xMaxTrap + (4.0*self.sigma)), self.iter)
        self.randY = np.random.uniform((-4.0*self.sigma), (self.yMaxTrap + (4.0*self.sigma)), self.iter)
        self.ntraps = len(self.xTrap)
#        print('ntraps', self.ntraps, 'max randXY', np.min(self.randX), np.min(self.randY), 
#            'ceil X', (self.xMaxTrap + (4.0*self.sigma)),
#            'ceil Y', (self.yMaxTrap + (4.0*self.sigma)))

        dist = distFX(self.randX, self.randY, self.xTrap, self.yTrap)

        pdect = self.g0 * np.exp(-(dist**2) / 2.0 / (self.sigma**2))

        dMask = dist <= 4.0*self.sigma

        PD = np.zeros(self.iter)
        nTraps = np.zeros(self.iter, dtype=int)
        for i in range(self.iter):
            m_i = dMask[i]
            pdect_i = pdect[i]
            PD[i] = 1.0 - np.prod(1.0 - pdect_i[m_i])
            nTraps[i] = np.sum(m_i)
        meanPD = np.mean(PD)
        quantPD = mquantiles(PD, prob=[0.025, 0.50, 0.975])
        print('mean =', meanPD, 'quants =', quantPD)
        ## Math PD
        trapArea = np.pi * (4.0 * self.sigma)**2
        trapDensity = nTraps / trapArea
        meanTrapDen = np.mean(trapDensity * 10000)
        densityQuant = mquantiles(trapDensity *10000, prob=[.25, .5, .75])
        print('total n traps', self.ntraps, 'nTraps', nTraps[:30], 
                'trapDen mean', meanTrapDen, 
                'trapDenQuant', densityQuant)

        trapBin = np.bincount(nTraps)
        print('trapBin', trapBin, 'trapArea', trapArea/10000)

        mathPD = 1.0 - np.exp(-2.0 * np.pi * self.g0 * (self.sigma**2) * trapDensity)
#        print('mathPD', mathPD[:10], 'trapDensity', (nTraps[:10] /trapArea))
        mathQuants = mquantiles(mathPD, prob=[0.025, 0.5, 0.975])
        print('MATH PD Mean = ', np.mean(mathPD), 'MATH PD Quants =', mathQuants)
        print('trapDensity', trapDensity[:30] *10000.0)


        sideArea = 1000. + (4*self.sigma)
        area = sideArea**2
        a = 2.0 * np.pi * self.g0 * self.sigma**2
#        print('a =', a, 'a per area =', a * self.ntraps / area)

        P.figure()
        P.plot(self.randX, self.randY, 'or')
        P.plot(self.xTrap, self.yTrap, 'xk')
        minXY = (-4.0*self.sigma) - 10.
        maxX = (4.0*self.sigma) + self.xMaxTrap + 10.0
        maxY = (4.0*self.sigma) + self.yMaxTrap + 10.0

#        print('minXY', minXY, 'maxX', maxX, 'maxY', maxY)

        P.xlim(minXY, maxX)
        P.ylim(minXY, maxY)
        P.show()



    
########            Main function
#######
def main():

    pdetect = PDetect() 


if __name__ == '__main__':
    main()
