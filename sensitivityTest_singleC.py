#!/usr/bin/env python

import sys
import pickle
import numpy as np
from kiwimodel import params
from kiwimodel import preProcessing
from kiwimodel import calculation

def unpickleAndRunModel(paramsPickleFname, preProcessingPickleName):
    # unpickle params
    fileobj = open(paramsPickleFname, 'rb')
    params = pickle.load(fileobj)
    fileobj.close()

    # preProcessing
    fileobj = open(preProcessingPickleName, 'rb')
    data = pickle.load(fileobj)
    fileobj.close()

    # run the model
    results = calculation.runModel(data, params)
    
    return params, data, results

def calculateOutputs(params, data, results, resultsFname):
    # re-calculate this - not stored in calculation.py
    kiwi_kMap = data.kiwiKMap

    # store the results
    resultData = {}

    # per mgmt zone
    mastingProportions = {}
    controlledProportions = {}
    zeroStoatProportions = {}
    meanStoatDensities = {}
    meanRodentDensities = {}
    kiwiGrowthRatios = {}
    kiwiGrowthRate5yrs = {}
    kiwiGrowthRate10yrs = {}
    kiwiGrowthRate20yrs = {}
    for yearn in range(len(params.years)):

        # rodent resolution - go through all control events
        # Dean - please check 
        for mask, startYear, revisit, shp in data.rodentControlList:
    
            # NOTE: popAllYears_3D only has data after burn in
            mastingMask = results.popAllYears_3D['MastT'][yearn] & mask
            prop = mastingMask.sum() / mask.sum()
            if shp in mastingProportions:
                mastingProportions[shp].append(prop)
            else:
                mastingProportions[shp] = [prop]
    
            controlMask = results.popAllYears_3D['ControlT'][yearn] & mask
            prop = controlMask.sum() / mask.sum()
            if shp in controlledProportions:
                controlledProportions[shp].append(prop)
            else:
                controlledProportions[shp] = [prop]

        # stoat resolution
        for i, key in enumerate(sorted(data.kiwiSpatialDictByMgmt.keys())):
            mask = data.kiwiSpatialDictByMgmt[key]
            zeroStoatMask = (results.popAllYears_3D['stoatDensity'][yearn] == 0) & mask
            prop = zeroStoatMask.sum() / mask.sum()
            if key in zeroStoatProportions:
                zeroStoatProportions[key].append(prop)
            else:
                zeroStoatProportions[key] = [prop]

    # Dean - please check
    # count from end so we don't need to worry about burn in etc
    # We only report the mean from the last 1/2 years of management
    # e.g. if we simulate 20 yrs of management, we report average stoat and rodent densities in the last 10 yrs
    densityYears = int(len(params.years) / 2)
    for i, key in enumerate(sorted(data.kiwiSpatialDictByMgmt.keys())):
        meanStoatDensities[key] = results.stoatDensity_2D[i][-densityYears:].mean()
        meanRodentDensities[key] = results.rodentDensity_2D[i][-densityYears:].mean()  
        kiwiGrowthRate5yrs[key] = np.log(results.kiwiDensity_2D[i, 54]/results.kiwiDensity_2D[i, params.burnin])/5
        kiwiGrowthRate10yrs[key] = np.log(results.kiwiDensity_2D[i, 59]/results.kiwiDensity_2D[i, params.burnin])/10
        kiwiGrowthRate20yrs[key] = np.log(results.kiwiDensity_2D[i, 69]/results.kiwiDensity_2D[i, params.burnin])/20
    # now the kiwi growth ratio (for all the areas)
    # this is calculated on a cell by cell basis
    # [-1] is the last yearor management, [0] is the first year of management
    # popAllYears_3D excludes burn in
    Bt = results.popAllYears_3D['kiwiDensity'][-1]
    B0 = results.popAllYears_3D['kiwiDensity'][0]
    tmp = kiwi_kMap - B0




    # There are a few situations where tmp is actually near
    # zero since k almost == B0. Not sure if this is the best way 
    # of handling this
    ignoreMask = ~data.kiwiExtentMask | (np.absolute(tmp) < 0.001)
    
    # so we don't get divide by 0 errors
    tmp[ignoreMask] = 1
    kiwiGrowthRatio = (Bt - B0) / tmp

    # kiwi growth ratio for all zones
    kiwiGrowthRatioMgmt = {}
    for key in sorted(data.kiwiSpatialDictByMgmt):
        mgmtMask = data.kiwiSpatialDictByMgmt[key]               #mask kiwi cells in mgmt zone
        sppMgmtMask = mgmtMask & data.kiwiExtentMask & ~ignoreMask
        kiwiGrowthRatioMgmt[key] = float(kiwiGrowthRatio[sppMgmtMask].mean())

    # do the means
    mastingProportionsRes = {}
    controlledProportionsRes = {}
    zeroStoatProportionsRes = {}
    for key in mastingProportions:
        mastingProportionsRes[key] = np.mean(mastingProportions[key])
        controlledProportionsRes[key] = np.mean(controlledProportions[key])
        zeroStoatProportionsRes[key] = np.mean(zeroStoatProportions[key])
        
    # save pickle
    resultData['meanPropMast'] = mastingProportionsRes
    resultData['meanPropControlled'] = controlledProportionsRes
    resultData['meanStoatDensities'] = meanStoatDensities
    resultData['meanRodentDensities'] = meanRodentDensities
    resultData['zeroStoatProportions'] = zeroStoatProportionsRes
    resultData['kiwiGrowthRatios'] = kiwiGrowthRatioMgmt
    resultData['kiwiGrowthRate5yrs'] = kiwiGrowthRate5yrs
    resultData['kiwiGrowthRate10yrs'] = kiwiGrowthRate10yrs
    resultData['kiwiGrowthRate20yrs'] = kiwiGrowthRate20yrs
    
    fileobj = open(resultsFname, 'wb')
    pickle.dump(resultData, fileobj)
    fileobj.close()

def runSingle(paramsPickleFname, preProcessingPickleName, resultsFname):
    params, data, results = (
            unpickleAndRunModel(paramsPickleFname, preProcessingPickleName))
    
    calculateOutputs(params, data, results, resultsFname)
    
    
if __name__ == '__main__':
    runSingle(sys.argv[1], sys.argv[2], sys.argv[3])
    
    
