#!/usr/bin/env python

import os
import pickle
import numpy as np
from pyDOE import lhs
from kiwimodel import params
from kiwimodel import preProcessing

from rios import rat

NREPS = 1000

PARAM_RANGES = [['mastPrEvent', None, 0.111, 0.333],
    ['reactivePropMgmtMasting', None, 0.2, 0.75],
    ['threshold_TT', None, 0.1, 0.75],
    ['deltaImmigrate', 0, 0.1, 1.4],
    ['deltaImmigrate', 1, 0.1, 1.4],
    ['deltaImmigrate', 2, 0.1, 1.4],
    ['rodentProbEatBait', None, 0.1, 0.85],
    ['pEatEncToxic', None, 0.5, 0.95],
    ['prpGrowRateControl', None, 0.1, 0.25]]


#    ['kOmega', None, 0.15, 0.35]]

NVARS = len(PARAM_RANGES)
    
def reScale(lowEnd, hiEnd, dat):
    minx = np.min(dat)
    maxx = np.max(dat)
    numerator = (hiEnd - lowEnd) * (dat - minx)
    denominator = maxx - minx
    newx = (numerator / denominator) + lowEnd
    return newx
    
def getLandscapeFactors(params, data):
    """
    Get the landscape factors as a dictionary.
    This only needs to be calculated once for all the reps
    """
    factors = {}
    
    # get the RAT 
    classNames = rat.readColumn(params.kClasses, "Class_Names")
    
    for name in [b"Beech", b"CoastalBeech"]:
        idx = np.nonzero(classNames == name)[0][0]
        classMask = data.kClasses == idx
        factors[name.decode()] = {}
        
        for (mask, startYear, revisit, shp) in data.rodentControlList:
            #mask = mask & data.kiwiExtentMask 
            maskAndClass = mask & classMask
            prop = maskAndClass.sum() / mask.sum()
            
            factors[name.decode()][shp] = prop * 100
            
    # now the elevation refuge
    factors['elevationRefuge'] = {}
    for (mask, startYear, revisit, shp) in data.rodentControlList:
        #mask = mask & data.kiwiExtentMask 
        elevRefuge = mask & (data.DEM <= params.kiwiMaxAltitude) & (data.DEM >= params.stoatMaxAltitude)
        prop = elevRefuge.sum() / mask.sum()
        factors['elevationRefuge'][shp] = prop * 100
        
    return factors

def generateParams():

    inputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'kiwi_data')
    outputDataPath = os.path.join(os.getenv('KIWIPROJDIR', default='.'), 'SensitivityResultsC')

    lhd = lhs(NVARS, samples=NREPS, criterion = 'm')
    print(lhd.shape)
    # rescale all
    for varIdx in range(NVARS):
        varName, subIdx, minVal, maxVal = PARAM_RANGES[varIdx]
        lhd[:, varIdx] = reScale(minVal, maxVal, lhd[:, varIdx])
        
    # we do the preProcessing on the first repetition
    # the data contained should be the same for every rep.
    data = None
    preProcessingPickleName = os.path.join(inputDataPath, 'kiwipreprocessing.pkl')
    landscapePickleName = os.path.join(inputDataPath, 'landscape.pkl')
    
    for rep in range(NREPS):
        # create a new params object for each iter
        pars = params.KiwiParams()
        ### SET DATA AND PATHS TO DIRECTORIES
        pars.setExtentShapeFile(os.path.join(inputDataPath, 'FiordlandConArea.shp'))
        pars.setKClasses(os.path.join(inputDataPath, 'seeds_RmIs_EqualBeech.img'))
#        pars.setKClasses(os.path.join(inputDataPath, 'rodentK_RmIslands.img'))
#        pars.setKClasses(os.path.join(inputDataPath, 'rodentK.img'))
        ### Area trapped in recent times.
#        pars.setIslands(os.path.join(inputDataPath, 'islandSecRes.tif'))
        pars.setIslands(os.path.join(inputDataPath, 'EmsTraps.tif'))
        pars.setDEM(os.path.join(inputDataPath, 'dem.tif'))
        pars.setResolutions((200.0, 1000.0, 1000.0))
        pars.setControlFile(os.path.join(inputDataPath, 'control3C.csv'))
        pars.setControlPathPrefix(inputDataPath)
        
        # set the years.
        pars.setBurnin(50)
        pars.setYears(range(20))
        
#        pars.setIslandK(2.0)
      
        for varIdx in range(NVARS):
            varName, subIdx, minVal, maxVal = PARAM_RANGES[varIdx]
            if not hasattr(pars, varName):
                raise ValueError('Invalid attribute name %s' % varName)
                
            if subIdx is None:
                # scalar
                setattr(pars, varName, lhd[rep, varIdx])
            else:
                # tuple
                tup = list(getattr(pars, varName))
                tup[subIdx] = lhd[rep, varIdx]
                setattr(pars, varName, tuple(tup))
                
            print(varIdx, getattr(pars, varName))
            
        # if we haven't done the preprocessing - do it now
        if data is None:
            data = preProcessing.KiwiData(pars)
            data.pickleSelf(preProcessingPickleName)
            
            # also save the landscape factors
            landscapeFactors = getLandscapeFactors(pars, data)
            fileobj = open(landscapePickleName, 'wb')
            pickle.dump(landscapeFactors, fileobj)
            fileobj.close()
        
        # create pickle file
#        paramsPickleName = os.path.join(inputDataPath, 'kiwiparams_%02d.pkl' % rep)
        paramsPickleName = os.path.join(outputDataPath, 'kiwiparams_%02d.pkl' % rep)
        resultName = os.path.join(outputDataPath, 'kiwiresults_%02d.pkl' % rep)
        fileobj = open(paramsPickleName, 'wb')
        pickle.dump(pars, fileobj)
        fileobj.close()
        # now fire up SLURM
        cmd = 'sbatch sensitivityTestC2.sl %s %s %s' % (paramsPickleName, 
                            preProcessingPickleName, resultName)
        print(cmd)
        os.system(cmd)

if __name__ == '__main__':
    generateParams()
