#!/usr/bin/env python

import sys
import numpy
from osgeo import gdal
from rios import rat
from rios import applier

def addClass(info, inputs, outputs):
    lowbeech = (inputs.lcdb[0] == 6) & (inputs.dem[0] < 200)
    newlcdb = numpy.where(lowbeech, 7, inputs.lcdb[0]).astype(numpy.uint8)
#    # make Resolution and Secretary Islands non habitat
#    islandMask = (inputs.islands[0] == 1) & (newlcdb > 0)
#    newlcdb = numpy.where(islandMask, 0, newlcdb)
    outputs.newlcdb = numpy.expand_dims(newlcdb, 0)


inputs = applier.FilenameAssociations()
inputs.lcdb = sys.argv[1]
inputs.dem = sys.argv[2]
#inputs.islands = sys.argv[3]

outputs = applier.FilenameAssociations()
outputs.newlcdb = sys.argv[3]

controls = applier.ApplierControls()
controls.setThematic(True)

applier.apply(addClass, inputs, outputs, controls=controls)

classNames = ["", "NonHabitat", "GrassScrub", "Shrub", "exoticForest", "broadLeaf", 
    "Beech", "CoastalBeech"]
rat.writeColumn(outputs.newlcdb, "Class_Names", classNames, colUsage=gdal.GFU_Name)

## CREATE SEED DENSITY MAP
### BELOW IS MAKES BEECH EQUAL EVERYWHERE
rat.writeColumn(outputs.newlcdb, "Rodent_CC", [0.0, 0.0,  75.0, 150.0, 300.0, 500.0, 
        450.0, 450.0])
### BELOW IS WITH INCREASED PRODUCTIVITY AT LOW ALTITUDE - COASTAL BEECH
#rat.writeColumn(outputs.newlcdb, "Rodent_CC", [0.0, 0.0,  75.0, 150.0, 300.0, 
#        450.0, 900.0])
### BELOW MAKES BEECH MAST MAX = 5000, INSTEAD OF 7500, TO REDUCE HUGE FLUXES
rat.writeColumn(outputs.newlcdb, "Rodent_MastCC", [0.0, 0.0, 75.0, 150.0, 300.0, 1000.0,
        5000.0, 5000.0])
#rat.writeColumn(outputs.newlcdb, "Rodent_MastCC", [0.0, 0.0, 75.0, 150.0, 300.0, 
#        7500.0, 7500.0])

## CREATE K CARRYING CAPACITY MAP
#rat.writeColumn(outputs.newlcdb, "Rodent_CC", [0.0, 0.0,  2.0, 3.0, 4.0, 5.0, 7.0])
#rat.writeColumn(outputs.newlcdb, "Rodent_MastCC", [0.0, 0.0, 2.0, 3.0, 4.0, 20.0, 20.0])
rat.writeColumn(outputs.newlcdb, "Masts", [0, 0, 0, 0, 0, 1, 1, 1])


#   command line call:
#   ./addlowbeechclass.py $POFPROJDIR/kiwi_data/lcdbINDX.img $POFPROJDIR/kiwi_data/dem.tif $POFPROJDIR/kiwi_data/islandSecRes.tif $POFPROJDIR/kiwi_data/rodentK.img


# ./addlowbeechclass.py $POFPROJDIR/kiwi_data/lcdbINDX.img $POFPROJDIR/kiwi_data/dem.tif $POFPROJDIR/kiwi_data/islandSecRes.tif $POFPROJDIR/kiwi_data/preBurninK.img $POFPROJDIR/kiwi_data/postBurninK.img

#   ./addlowbeechclass.py kiwi_data/lcdbINDX.img kiwi_data/dem.tif kiwi_data/islandSecRes.tif kiwi_data/lcdb_lowbeech.img

#   ./addlowbeechclass.py kiwi_data/lcdbINDX.img kiwi_data/dem.tif kiwi_data/islandSecRes.tif kiwi_data/rodentK_RmIslands.img

### CALL TO MAKE COASTAL BEECH
#   ./addlowbeechclass.py kiwi_data/lcdbINDX.img kiwi_data/dem.tif kiwi_data/islandSecRes.tif kiwi_data/seeds_RmIslands.img

### CALL TO MAKE BEECH EQUAL EVERYWHERE
#   ./addlowbeechclass.py kiwi_data/lcdbINDX.img kiwi_data/dem.tif kiwi_data/islandSecRes.tif kiwi_data/seeds_RmIs_EqualBeech.img

### CALL TO MAKE BEECH MAST REDUCED TO 5000
#   ./addlowbeechclass.py kiwi_data/lcdbINDX.img kiwi_data/dem.tif kiwi_data/islandSecRes.tif kiwi_data/seeds_Beech5000.img

### CALL FOR KEA
#   ./addlowbeechclass.py kea/keaData/lcdb_kea_indx.img kea/keaData/seedsRegion3.
